#ifndef SHADER_H
#define SHADER_H

#include <QOpenGLFunctions>
#include <string>

class Shader
{
public:
    Shader();

    void setVertexShaderSource(std::string str);
    void setFragmentShaderSource(std::string str);
    void build();
    unsigned int getShaderProgramId();

private:
    std::string   vertexShaderSource;
    std::string fragmentShaderSource;
    GLuint vertexShaderId;
    GLuint fragmentShaderId;
    GLuint shaderProgramId;
};

#endif // SHADER_H
