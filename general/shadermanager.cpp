#include "shadermanager.h"

#include <QString>
#include <QDebug>
#include <string>
#include <iostream>
#include <fstream>
#include <QFile>
#include <QDir>

ShaderManager::ShaderManager()
{
    QFile file;
    std::string source_v1;
    std::string source_f1;

    QDir::setCurrent("/home/fugrai/Documents/ws/qt/SpiderProbe/res/shaders");

    //shaderConstant

    Shader *shaderConstant = new Shader;
    shaderList.push_back(shaderConstant);

    //shaderVertexColor

    Shader *shaderVertexColor = new Shader;
    shaderList.push_back(shaderVertexColor);

    source_v1.clear();
    source_f1.clear();

    file.setFileName("shaderVertexColor.vert");
    file.open(QIODevice::ReadOnly);
    source_v1.append(file.readAll());
    file.close();
    file.flush();
    //qDebug() << source_v1.c_str();

    file.setFileName("shaderVertexColor.frag");
    file.open(QIODevice::ReadOnly);
    source_f1.append(file.readAll());
    file.close();
    file.flush();
    //qDebug() << source_f1.c_str();

    shaderVertexColor->setVertexShaderSource(source_v1);
    shaderVertexColor->setFragmentShaderSource(source_f1);

    //shaderGrid
    Shader *shaderGrid = new Shader;
    shaderList.push_back(shaderGrid);

    source_v1.clear();
    source_f1.clear();

    file.setFileName("shaderGrid.vert");
    file.open(QIODevice::ReadOnly);
    source_v1.append(file.readAll());
    file.close();

    file.setFileName("shaderGrid.frag");
    file.open(QIODevice::ReadOnly);
    source_f1.append(file.readAll());
    file.close();

    shaderGrid->setVertexShaderSource(source_v1);
    shaderGrid->setFragmentShaderSource(source_f1);

    //shaderBone
    Shader *shaderBone = new Shader;
    shaderList.push_back(shaderBone);

    source_v1.clear();
    source_f1.clear();

    file.setFileName("shaderBone.vert");
    file.open(QIODevice::ReadOnly);
    source_v1.append(file.readAll());
    file.close();

    file.setFileName("shaderBone.frag");
    file.open(QIODevice::ReadOnly);
    source_f1.append(file.readAll());
    file.close();

    shaderBone->setVertexShaderSource(source_v1);
    shaderBone->setFragmentShaderSource(source_f1);

    qDebug() << "shader manager constructor lefutott";

}

void ShaderManager::buildSahders()
{
    qDebug() << "number of shaders:" << shaderList.length();
    for(int i=0; i<shaderList.length(); i++)
    {
        shaderList[i]->build();
    }
    //shaderList[1]->build();
}

int ShaderManager::getShader(int shader)
{
    return shaderList[shader]->getShaderProgramId();
}
