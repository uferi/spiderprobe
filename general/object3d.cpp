#include "object3d.h"

Object3D::Object3D()
{
    _parent = nullptr;
}

void Object3D::initObject()
{
    for(int i=0; i<_child.length(); i++)
    {
        _child[i]->initObject();
    }
}

void Object3D::paintObject(QMatrix4x4 parentMatrix)
{
    QMatrix4x4 localMatrix;
    localMatrix = parentMatrix * _mModel;
    for(int i=0; i<_child.length(); i++)
    {
        _child[i]->paintObject(localMatrix);
    }
}

void Object3D::setParentObject(Object3D *parent)
{
    _parent = parent;
    parent->addChild(this);
}

void Object3D::addChild(Object3D *child)
{
    _child.append(child);
}

Object3D *Object3D::findChild(QString name)
{
    Object3D *foundObject;

    if( _name == name )
    {
        foundObject = this;
    }
    else
    {
        int i=0;
        while(!foundObject && i<_child.length() )
        {
            foundObject = _child[i]->findChild(name);
            i++;
        }
    }
    return foundObject;
}

void Object3D::setObjectName(QString name)
{
    _name = name;
}
