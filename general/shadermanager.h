#ifndef SHADERMANAGER_H
#define SHADERMANAGER_H

#include <QVector>

#include "general/shader.h"


class ShaderManager
{
public:
    //enum shaderType : unsigned int
    //{
    //    SHADER_CONSTANT ,
    //    SHADER_VERTEX_COLOR,
    //    SHADER_MAJOM
    //};

    ShaderManager();
    void buildSahders();
    int getShader(int shader);

protected:
    QVector<Shader*> shaderList;
};

#endif // SHADERMANAGER_H
