#include "shader.h"

#include <QDebug>
#include <string>

Shader::Shader()
{

}

void Shader::setVertexShaderSource(std::string str)
{
    vertexShaderSource = str;
}

void Shader::setFragmentShaderSource(std::string str)
{
    fragmentShaderSource += str;
}

void Shader::build()
{

    qDebug() << "shader.build called";
    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();

    //qDebug() << "vertex source:" << vertexShaderSource.c_str();

    // create vertex shader
    vertexShaderId = f->glCreateShader(GL_VERTEX_SHADER);
    const char* tmp_str_vertex = vertexShaderSource.c_str();
    f->glShaderSource(vertexShaderId, 1, &tmp_str_vertex, NULL);
    f->glCompileShader(vertexShaderId);

    int success;
    char infoLog[512];
    f->glGetShaderiv(vertexShaderId, GL_COMPILE_STATUS, &success);
    qDebug() << "vertex shader Id:" << vertexShaderId << "success:" << success;
    if (!success){
        f->glGetShaderInfoLog(vertexShaderId, 512, NULL, infoLog);
        qDebug() << "infoLog" << infoLog;
    }


    // create fragment shader
    fragmentShaderId = f->glCreateShader(GL_FRAGMENT_SHADER);
    const char* tmp_str_fragment = fragmentShaderSource.c_str();
    f->glShaderSource(fragmentShaderId, 1, &tmp_str_fragment, NULL);
    f->glCompileShader(fragmentShaderId);

    f->glGetShaderiv(fragmentShaderId, GL_COMPILE_STATUS, &success);
    qDebug() << "fragment shader Id:" << fragmentShaderId << " success:" << success ;
    if (!success){
        f->glGetShaderInfoLog(fragmentShaderId, 512, NULL, infoLog);
        qDebug() << "infoLog" << infoLog;
    }


    // create shader program
    shaderProgramId = f->glCreateProgram();
    f->glAttachShader(shaderProgramId, vertexShaderId);
    f->glAttachShader(shaderProgramId, fragmentShaderId);
    f->glLinkProgram(shaderProgramId);

    f->glGetShaderiv(shaderProgramId, GL_LINK_STATUS, &success);
    qDebug() << "shaderProgram id:"<< shaderProgramId << " link success:" << success ;
    if (!success){
        f->glGetProgramInfoLog(shaderProgramId, 512, NULL, infoLog);
        qDebug() << "infoLog" << infoLog;
    }

    f->glDeleteShader(vertexShaderId);
    f->glDeleteShader(fragmentShaderId);
}

unsigned int Shader::getShaderProgramId()
{
    return shaderProgramId;
}
