#ifndef ENTITY_H
#define ENTITY_H

#include <QVector3D>
#include <QMatrix4x4>

class Entity
{
public:
    Entity();

    void translate(QVector3D vec);
    void rotate (QVector3D vec);

public:
    QVector3D pos;
    QVector3D rot;
    QMatrix4x4 mTransform;

};

#endif // ENTITY_H
