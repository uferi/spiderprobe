#ifndef NODE3D_H
#define NODE3D_H

#include "general/object3d.h"

#include <QString>
#include <QVector>

class Node3d
{
public:
    Node3d();

    QString name;
    Node3d *parent;
    QVector<Node3d*> child;

private:

};

#endif // NODE3D_H
