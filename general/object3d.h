#ifndef OBJECT3D_H
#define OBJECT3D_H

#include <vector>
#include <string>

#include <QOpenGLFunctions>
#include <QMatrix4x4>

//enum Object3D::shaderType
//{
//    CONSTANT,
//    VERTEX_COLOR
//}

class Object3D : public QOpenGLFunctions
{
public:
    Object3D();

    enum ShaderType
    {
        CONSTANT,
        VERTEX_COLOR,
        GRID,
        BONE
    };

    virtual void initObject();
    virtual void paintObject(QMatrix4x4 parentMatrix);

    virtual void setParentObject(Object3D* parent);
    virtual void addChild(Object3D* child);
    virtual Object3D* findChild(QString name);
    virtual void setObjectName(QString name);

    bool _visible;

    std::vector<float> _data;

    GLuint _VBO;
    GLuint _vertexshader;
    GLuint _fragmentShader;
    GLuint _shaderProgram;
    GLuint _VAO;

    QMatrix4x4 _mModel;

    QString _name;
    Object3D* _parent;
    QVector<Object3D*> _child;
};

#endif // OBJECT3D_H
