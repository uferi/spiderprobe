#include "scenemanager.h"
#include "general/shadermanager.h"
#include "glwidget.h"
#include <QDebug>

#include <math.h>



//#include "spider/bone.h"

//QOpenGLWidget openglWidget;

SceneManager::SceneManager(QObject *parent) : QObject(parent)
{
    qDebug() << "sceneManager instatiated";

    _running = true;

    masterFps = 480;
    //masterFps = 240;
    //masterFps = 60;


    shaderManager = new ShaderManager;

    root = new Object3D;
    root->setObjectName("root");

    envManager = new EnvManager(nodeList, root);


    bone = new Bone(1);
    bone->setObjectName(QString("bone"));
    qDebug() << "bone name:::" << bone->_name;
    bone->setParentObject(root);
    nodeList.append(bone);

    qDebug() << "root child amount: " << root->_child.length();
    for(int i=0; i<root->_child.length(); i++)
    {
        qDebug() << root->_child[i]->_name;
    }

    bone2 = new Bone(1);
    bone2->setObjectName(QString("bone2"));
    bone2->setParentObject(bone);
    nodeList.append(bone2);

    bone3 = new Bone(2);
    bone3->setObjectName(QString("bone3"));
    bone3->setParentObject(bone2);
    nodeList.append(bone3);

    bone4 = new Bone(1.5);
    bone4->setObjectName(QString("bone4"));
    bone4->setParentObject(bone3);
    //bone4->setParentObject(root);
    nodeList.append(bone4);


    loc1 = new Locator(1.0);
    loc1->setObjectName(QString("loc1"));
    loc1->setParentObject(bone4);
    nodeList.append(loc1);

    cameraManager = new CameraManager;

    cameraManager->createCamera("cam1");
    cameraManager->setRenderCamera("cam1");
    //Camera* renderCam = cameraManager->getRenderCamera();



    spider = new Spider(root);
    //nodeList.append(spider);

    //DashedLine *testLine = new DashedLine(QVector3D(0,0,0),QVector3D(150,0,150));
    //testLine->setParentObject(root);

    // ---- BecierCurve test
    QVector3D pt0 = QVector3D(0,0,0);
    QVector3D pt1 = QVector3D(30,0,0);
    QVector3D pt2 = QVector3D(20,0,60);
    QVector3D pt3 = QVector3D(60,0,40);
    BezierCurve *bezier = new BezierCurve(pt0,pt1,pt2,pt3);
    bezier->setParentObject(root);

    QVector3D pt4 = QVector3D(60,0,40);
    QVector3D pt5 = QVector3D(100,0,20);
    QVector3D pt6 = QVector3D(60,0,-10);
    QVector3D pt7 = QVector3D(50,0,0);
    BezierCurve *bezier2 = new BezierCurve(pt4,pt5,pt6,pt7);
    bezier2->setParentObject(root);

    //------ gamepad ------
    auto gamepads = QGamepadManager::instance()->connectedGamepads();
    if( gamepads.isEmpty() )
    {
        qDebug() << "Could not find any gamepad";
    }
    _gamepad = new QGamepad(*gamepads.begin(), this);
    //connect(_gamepad, &QGamepad::axisLeftXChanged, this, [](double value){
    //    qDebug() << "Left X" << value;
    //});
    connect(_gamepad, SIGNAL(axisRightXChanged(double)), this, SLOT(rotateSpiderX(double)));

    //connect(_gamepad, &QGamepad::axisLeftYChanged, this, [](double value){
    //    qDebug() << "Left Y" << value;
    //});
    connect(_gamepad, SIGNAL(axisRightYChanged(double)), this, SLOT(rotateSpiderY(double)));

    //connect(_gamepad, &QGamepad::axisRightXChanged, this, [](double value){
    //    qDebug() << "Right X" << value;
    //});
    connect(_gamepad, SIGNAL(axisLeftXChanged(double)), this, SLOT(moveSpiderX(double)));

    //connect(_gamepad, &QGamepad::axisRightYChanged, this, [](double value){
    //    qDebug() << "Right Y" << value;
    //});
    connect(_gamepad, SIGNAL(axisLeftYChanged(double)), this, SLOT(moveSpiderZ(double)));

    //connect(_gamepad, &QGamepad::buttonAChanged, this, [](bool pressed){
    //    qDebug() << "Button A" << pressed;
    //});
    connect(_gamepad, SIGNAL(buttonAChanged(bool)), this, SLOT(resetBody(bool)));

    connect(_gamepad, &QGamepad::buttonR2Changed, this, [](double value){
        qDebug() << "Button R2" << value;
    });
    connect(_gamepad, &QGamepad::buttonL2Changed, this, [](double value){
        qDebug() << "Button L2" << value;
    });
    //------ gamepad END ------

    connect(&masterTimer, SIGNAL(timeout()), this, SLOT(masterUpdate()));



    //masterTimer.start(1000/60);
    masterTimer.start(1000/masterFps);
    //masterTimer.start(1000/480);
    //masterTimer.start(0);

    fpsCount = 0;
    //fpsTime.start();
    //time.start();


    //// -------- IK solve test --------
    //ikbone1 = new Bone(60,QVector3D(0.8,0.8,0.2));
    //ikbone1->setParentObject(root);
    //ikbone2 = new Bone(90,QVector3D(0.8,0.8,0.2));
    //ikbone2->setParentObject(ikbone1);
    //iklocend = new Locator(50.,QVector3D(0,0,0));
    //iklocend->setParentObject(root);
    //iklocend->_mModel.translate(-30,20,110);
    //
    //QVector3D ikTargetPos = QVector3D(iklocend->_mModel.data()[12],iklocend->_mModel.data()[13],iklocend->_mModel.data()[14]);
    //qDebug() << ikTargetPos;
    //QMatrix4x4 matIkRoot;
    //matIkRoot.setToIdentity();
    //matIkRoot.lookAt(QVector3D(0,0,0), ikTargetPos, QVector3D(0,0.1,0));
    //
    //float angleRad = atan2(ikTargetPos.x(), ikTargetPos.z());
    //float angleDeg = (angleRad/M_PI)*180;
    //
    //ikbone1->_mModel.setToIdentity();
    ////ikbone1->_mModel = matIkRoot;
    //ikbone1->_mModel.rotate(angleDeg,0,1,0);
    //// -------- IK solve test ----END----


}

SceneManager &SceneManager::instance()
{
    static SceneManager *instance_;
    if( instance_ == nullptr )
    {
        instance_ = new SceneManager();
    }
    return *instance_;
}

SceneManager::~SceneManager()
{
    //delete envManager;
    //delete spider;
}

EnvManager *SceneManager::getEnv()
{
    return envManager;
}

Spider *SceneManager::getSpider()
{
    return spider;
}

void SceneManager::buildShaders()
{
    shaderManager->buildSahders();
}

int SceneManager::getShader(int shader)
{
    shaderManager->getShader(shader);
}

Camera *SceneManager::getRenderCamera()
{
    return cameraManager->getRenderCamera();
}

Object3D *SceneManager::getObject(QString name)
{
    Object3D* foundObject;

    if(root->_name == name)
    {
        foundObject = root;
    }else
    {
        //foundObject = root->findChild(name);
    }

    if(!foundObject)
    {
        qDebug() << "object with name:" << name << " not found!";
    }
    return foundObject;
}

void SceneManager::setProjMatrix(QMatrix4x4 matrix)
{
    projMatrix = matrix;
}

QMatrix4x4 SceneManager::getProjMatrix()
{
    return projMatrix;
}

int SceneManager::getMasterFps()
{
    return masterFps;
}

void SceneManager::renderScene()
{
    QMatrix4x4 parentMatrix;
    parentMatrix.setToIdentity();
    root->paintObject(parentMatrix);
}

void SceneManager::initScene()
{
    root->initObject();
}

void SceneManager::toggleRunning()
{
    if(_running)
    {
        _running = false;
    }
    else
    {
        _running = true;
    }
    spider->toggleTimer();
}

void SceneManager::testDraw()
{
    //QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();
    //glFunc->glGenBuffers(1, &nodeList[0]->_VBO);
    //glFunc->glBindBuffer(GL_ARRAY_BUFFER, nodeList[0]->_VBO);
    //glFunc->glBufferData(GL_ARRAY_BUFFER, nodeList[0]->_data.size()*4, nodeList[0]->_data.data(), GL_DYNAMIC_DRAW);
    //glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    //glFunc->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)12);
}

QVector3D SceneManager::getTranslate(QMatrix4x4 matrix)
{
    QVector3D result;

    result.setX(matrix.data()[12]);
    result.setY(matrix.data()[13]);
    result.setZ(matrix.data()[14]);

    return result;
}

void SceneManager::masterUpdate()
{
    fpsCount++;

    //qDebug() << "yo";
    //qDebug() << nodeList.length();

    // user input

    // update based on user input

    // sim kind of things

    // update based on sims

    // collision detection

    // display update
    //GLWidget::createObject();


    //qDebug() << time.elapsed() << "  " << fpsCount;

    //if(fpsTime.elapsed()>1000)
    //{
    //    qDebug() << fpsCount;
    //    fpsTime.restart();
    //    fpsCount = 0;
    //}
    //
    //if(time.elapsed()>1000/60)
    //{
    //    time.restart();
    //
    //    bone->_mModel.rotate( 0.5,-1,0,0);
    //    bone2->_mModel.rotate(0.5,-1,0,0);
    //    bone3->_mModel.rotate(0.5,-1,0,0);
    //    bone4->_mModel.rotate(3.0, 0,0,1);
    //}

    if(_running)
    {
        bone->_mModel.rotate( 0.5,-1,0,0);
        bone2->_mModel.rotate(-0.5,-1,0,0);
        bone3->_mModel.rotate(0.5,-1,0,0);
        bone4->_mModel.rotate(3.0, 0,0,1);

        spider->rotate(QVector3D(0,axisLeftX,0));
        spider->rotate(QVector3D(axisLeftY,0,0));
        spider->move(-spiderMoveVector);

        //float tempLength = bone3->getLength();
        //tempLength += 0.1;
        //bone3->resizeBone(tempLength);
        //bone3->setColor(QVector3D(0.9,0.25,0.1));

        spider->solveIK();
    }


}

void SceneManager::setSegALength(int value)
{
    spider->setSegALength( (float)value );
}

void SceneManager::setSegBLength(int value)
{
    spider->setSegBLength(value);
}

void SceneManager::setSegCLength(int value)
{
    spider->setSegCLength(value);
}

void SceneManager::setRootXPos(int value)
{
    spider->setRootXPos(value);
}

void SceneManager::setRootZPos(int value)
{
    spider->setRootZPos(value);
}

void SceneManager::setRootRestAngle(int value)
{
    spider->setRootRestAngle(value);
}

void SceneManager::setBodyHeight(int value)
{
    spider->setBodyHeight(value);
}

void SceneManager::rotateSpiderX(double angle)
{
    //spider->rotate(QVector3D(0,angle,0));
    //qDebug() << "scnMng rotateSpiderX";
    axisLeftX = angle * 180 / masterFps;
}

void SceneManager::rotateSpiderY(double angle)
{
    //spider->rotate(QVector3D(angle,0,0));
    axisLeftY = angle * 60.0 / masterFps;
}

void SceneManager::moveSpiderX(double value)
{
    spiderMoveVector.setX( (float)value * 120 / masterFps) ;
}

void SceneManager::moveSpiderY(double value)
{
    spiderMoveVector.setY( (float)value*60/ masterFps);
}

void SceneManager::moveSpiderZ(double value)
{
    spiderMoveVector.setZ( (float)value * 120 / masterFps);
}

void SceneManager::resetBody(bool button)
{
    if(button)
    {
        spider->resetBody();
    }
}
