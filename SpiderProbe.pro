#-------------------------------------------------
#
# Project created by QtCreator 2018-11-12T13:20:05
#
#-------------------------------------------------

QT       += core gui
QT  += gamepad
QT_GAMEPAD_DEVICE = /dev/input/js0

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SpiderProbe
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    env/camera.cpp \
    env/env.cpp \
    env/envmanager.cpp \
    env/grid.cpp \
    general/entity.cpp \
    general/node3d.cpp \
    general/object.cpp \
    general/object3d.cpp \
    general/shader.cpp \
    general/shadermanager.cpp \
    spider/leg.cpp \
    spider/segment.cpp \
    spider/spider.cpp \
    glwidget.cpp \
    main.cpp \
    mainwindow.cpp \
    scenemanager.cpp \
    env/origingnomon.cpp \
    env/cameramanager.cpp \
    env/backgroundgradient.cpp \
    spider/bone.cpp \
    env/locator.cpp \
    env/distancegauge.cpp \
    env/dashedline.cpp \
    env/beziercurve.cpp \
    spider/stepcurve.cpp

HEADERS += \
    env/camera.h \
    env/env.h \
    env/envmanager.h \
    env/grid.h \
    general/entity.h \
    general/node3d.h \
    general/object.h \
    general/object3d.h \
    general/shader.h \
    general/shadermanager.h \
    spider/leg.h \
    spider/segment.h \
    spider/spider.h \
    entity.h \
    glwidget.h \
    mainwindow.h \
    scenemanager.h \
    env/origingnomon.h \
    env/cameramanager.h \
    env/backgroundgradient.h \
    spider/bone.h \
    env/locator.h \
    env/distancegauge.h \
    env/dashedline.h \
    env/beziercurve.h \
    spider/stepcurve.h


FORMS += \
        mainwindow.ui

DISTFILES += \
    res/shaders/shaderVertexColor.frag \
    res/shaders/shaderVertexColor.vert \
    res/shaders/questions.txt \
    res/shaders/shaderGrid.vert \
    res/shaders/shaderGrid.frag \
    res/shaders/shaderBone.frag \
    res/shaders/shaderBone.vert

