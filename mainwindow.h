#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "glwidget.h"

#include "scenemanager.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void connectValues(SceneManager *scnMng);

    GLWidget *glWidget;
private:
    Ui::MainWindow *ui;
    //GLWidget *glWidget;
};

#endif // MAINWINDOW_H
