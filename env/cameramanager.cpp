#include "cameramanager.h"

CameraManager::CameraManager()
{

}

void CameraManager::createCamera(QString camName)
{
    Camera* newCamera = new Camera( QVector3D(180*1.5,120*1.5,180*1.5), QVector3D(0,0,0) );
    newCamera->_name = camName;
    _cameraList.push_back(newCamera);
    //_cameraMap[camName] = newCamera;
    _cameraMap.insert(std::make_pair( camName, newCamera));
}

void CameraManager::setRenderCamera(QString camName)
{
    std::map<QString, Camera*>::iterator it = _cameraMap.find(camName);
    _renderCamera = it->second;
}

Camera *CameraManager::getRenderCamera()
{
    return _renderCamera;
}
