#ifndef GRID_H
#define GRID_H

#include <vector>

//#include <QOpenGLExtraFunctions>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QVector4D>
#include <QString>

#include "general/entity.h"
#include "general/object3d.h"
#include "general/node3d.h"
#include "general/shader.h"

class Grid : public Entity, public Object3D
{
public:
    Grid(int size, int division, int fadeSize);

    void initObject() override;
    void paintObject(QMatrix4x4 parentMatrix) override;

private:
    void addPointToData( QVector4D point, float alignment);

    int _size;
    int _division;
    int _fadeSize;

    unsigned int position_attrib_index;
    unsigned int opacity_attrib_index;
    unsigned int alignment_attrib_index;

    //QOpenGLFunctions *openGLFunctions;

};

#endif // GRID_H
