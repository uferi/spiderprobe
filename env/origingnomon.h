#ifndef ORIGINGNOMON_H
#define ORIGINGNOMON_H

#include "general/object3d.h"

class OriginGnomon : public Object3D
{
public:
    OriginGnomon(int size_in);

    void initObject() override;
    void paintObject(QMatrix4x4 parentMatrix) override;

private:
    void addPoint(QVector3D pos, QVector3D col);

    float size;
};

#endif // ORIGINGNOMON_H
