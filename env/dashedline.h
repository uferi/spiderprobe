#ifndef DASHEDLINE_H
#define DASHEDLINE_H

#include "general/object3d.h"

class DashedLine : public Object3D
{
public:
    DashedLine(QVector3D start, QVector3D end);

    void initObject() override;
    void paintObject(QMatrix4x4 parentMatrix) override;

    void updateLine(QVector3D start, QVector3D end);

private:
    bool _initialised;
    float _length;
    float _dashLength;
    QVector3D _startPos;
    QVector3D _endPos;
    QVector3D _color;

    void addPoint(QVector3D pos, QVector3D col);
    void buildData();
};

#endif // DASHEDLINE_H
