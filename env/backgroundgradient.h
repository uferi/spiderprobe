#ifndef BACKGROUNDGRADIENT_H
#define BACKGROUNDGRADIENT_H

#include "general/object3d.h"

class BackgroundGradient : public Object3D
{
public:
    BackgroundGradient();

    void initObject() override;
    void paintObject(QMatrix4x4 parentMatrix) override;

    void addPoint(QVector3D pos, QVector3D col);
};

#endif // BACKGROUNDGRADIENT_H
