#ifndef CAMERAMANAGER_H
#define CAMERAMANAGER_H

#include "env/camera.h"

#include <vector>
#include <map>
#include <string>

class CameraManager
{
public:
    CameraManager();

    void createCamera(QString camName);
    void setRenderCamera(QString camName);
    Camera* getRenderCamera();

private:
    Camera* _renderCamera;
    std::vector<Camera*> _cameraList;
    std::map<QString, Camera*> _cameraMap;
};

#endif // CAMERAMANAGER_H
