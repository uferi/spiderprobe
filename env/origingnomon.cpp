#include "origingnomon.h"

#include "scenemanager.h"


OriginGnomon::OriginGnomon(int size_in)
{
    size = size_in;
    //size = 5;
    // X - axis
    addPoint(QVector3D(0   ,0,0), QVector3D(1,0,0));
    addPoint(QVector3D(size,0,0), QVector3D(1,0,0));

    // Y - axis
    addPoint(QVector3D(0,0   ,0), QVector3D(0,1,0));
    addPoint(QVector3D(0,size,0), QVector3D(0,1,0));

    // Z - axis
    addPoint(QVector3D(0,0,0   ), QVector3D(0,0,1));
    addPoint(QVector3D(0,0,size), QVector3D(0,0,1));
}

void OriginGnomon::initObject()
{
    SceneManager& scnMng = SceneManager::instance();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();
    glFunc->glGenBuffers(1, &_VBO);
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glBufferData(GL_ARRAY_BUFFER, _data.size()*4, _data.data(), GL_STATIC_DRAW);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)12);

    _shaderProgram = scnMng.getShader(ShaderType::VERTEX_COLOR);

    for(int i=0; i<_child.length(); i++)
    {
        _child[i]->initObject();
    }

}

void OriginGnomon::paintObject(QMatrix4x4 parentMatrix)
{
    //qDebug() << "the origin gnomon draws itself";
    SceneManager& scnMng = SceneManager::instance();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();

    glFunc->glUseProgram(_shaderProgram);

    // bind VBO first !!!
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)12);
    glFunc->glEnableVertexAttribArray(0);
    glFunc->glEnableVertexAttribArray(1);

    QMatrix4x4 modelMatrix;
    QMatrix4x4 projMatrix = scnMng.getProjMatrix();

    unsigned int modelMatrixLoc;
    unsigned int camMatrixLoc;
    unsigned int projMatrixLoc;

    modelMatrix.setToIdentity();
    modelMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"modelMatrix");
    camMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"camMatrix");
    projMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"projMatrix");

    Camera* renderCamera = scnMng.getRenderCamera();
    QMatrix4x4 camMatrix;
    camMatrix = renderCamera->getMatrix();

    glFunc->glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, (GLfloat*)&modelMatrix );
    glFunc->glUniformMatrix4fv(camMatrixLoc, 1, GL_FALSE, (GLfloat*)&camMatrix );
    glFunc->glUniformMatrix4fv(projMatrixLoc, 1, GL_FALSE, (GLfloat*)&projMatrix);

    glFunc->glLineWidth(1);

    //glDrawArrays(GL_TRIANGLES, 0, 3);
    glFunc->glDrawArrays(GL_LINES, 0, _data.size()/6);
    //glPointSize(5.0);
    //glDrawArrays(GL_POINTS, 0, scnMng.nodeList[0]->_data.size()/5);
}

void OriginGnomon::addPoint(QVector3D pos, QVector3D col)
{
    _data.push_back( pos.x() );
    _data.push_back( pos.y() );
    _data.push_back( pos.z() );

    _data.push_back( col.x() );
    _data.push_back( col.y() );
    _data.push_back( col.z() );
}
