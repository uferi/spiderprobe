#include "dashedline.h"

#include "scenemanager.h"

DashedLine::DashedLine(QVector3D start, QVector3D end)
{
    _initialised = false;
    _color = QVector3D(0.3,0.5,0.3);
    _dashLength = 5.0;

    _startPos = start;
    _endPos = end;

    buildData();
}

void DashedLine::initObject()
{
    SceneManager& scnMng = SceneManager::instance();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();
    glFunc->glGenBuffers(1, &_VBO);
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glBufferData(GL_ARRAY_BUFFER, _data.size()*4, _data.data(), GL_STATIC_DRAW);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)12);

    _shaderProgram = scnMng.getShader(ShaderType::VERTEX_COLOR);

    for(int i=0; i<_child.length(); i++)
    {
        _child[i]->initObject();
    }
    _initialised = true;
}

void DashedLine::paintObject(QMatrix4x4 parentMatrix)
{
    //qDebug() << "the dashed line draws itself";

    SceneManager& scnMng = SceneManager::instance();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();

    glFunc->glUseProgram(_shaderProgram);

    // bind VBO first !!!
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)12);
    glFunc->glEnableVertexAttribArray(0);
    glFunc->glEnableVertexAttribArray(1);

    QMatrix4x4 modelMatrix;
    QMatrix4x4 projMatrix = scnMng.getProjMatrix();

    unsigned int modelMatrixLoc;
    unsigned int camMatrixLoc;
    unsigned int projMatrixLoc;

    modelMatrix =  parentMatrix * _mModel;
    modelMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"modelMatrix");
    camMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"camMatrix");
    projMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"projMatrix");

    Camera* renderCamera = scnMng.getRenderCamera();
    QMatrix4x4 camMatrix;
    camMatrix = renderCamera->getMatrix();

    glFunc->glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, (GLfloat*)&modelMatrix );
    glFunc->glUniformMatrix4fv(camMatrixLoc, 1, GL_FALSE, (GLfloat*)&camMatrix );
    glFunc->glUniformMatrix4fv(projMatrixLoc, 1, GL_FALSE, (GLfloat*)&projMatrix);

    glFunc->glLineWidth(1.5);
    glFunc->glDrawArrays(GL_LINES, 0, _data.size()/6);

    QMatrix4x4 localMatrix;
    localMatrix = parentMatrix * _mModel;
    //localMatrix.translate(0,0,_length);       // only bone has this offset
    for(int i=0; i<_child.length(); i++)
    {
        _child[i]->paintObject(localMatrix);
    }
}

void DashedLine::updateLine(QVector3D start, QVector3D end)
{
    _startPos = start;
    _endPos = end;

    _data.clear();

    buildData();

    if(_initialised)
    {
    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();
    //glFunc->glGenBuffers(1, &_VBO);
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glBufferData(GL_ARRAY_BUFFER, _data.size()*4, _data.data(), GL_STATIC_DRAW);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)12);
    }
}

void DashedLine::addPoint(QVector3D pos, QVector3D col)
{
    _data.push_back( pos.x() );
    _data.push_back( pos.y() );
    _data.push_back( pos.z() );

    _data.push_back( col.x() );
    _data.push_back( col.y() );
    _data.push_back( col.z() );
}

void DashedLine::buildData()
{
    QVector3D startToEnd;
    startToEnd = _endPos - _startPos;

    _length = startToEnd.length();
    QVector3D dirVec;
    dirVec = startToEnd.normalized();

    int dashAmount = _length/(_dashLength*2);
    QVector3D pieceStart;
    pieceStart = _startPos;
    QVector3D pieceEnd;
    pieceEnd = pieceStart+(dirVec*_dashLength);
    for(int i=0; i<dashAmount; i++)
    {
        addPoint(pieceStart, _color);
        addPoint(pieceEnd, _color);

        pieceStart = pieceStart + ( dirVec * _dashLength * 2);
        pieceEnd = pieceEnd + ( dirVec * _dashLength * 2);
    }

    pieceStart = _endPos + (dirVec * -1 * _dashLength);
    pieceEnd = _endPos;

    pieceStart = pieceStart + ( dirVec * _dashLength * 2);
    pieceEnd = pieceEnd + ( dirVec * _dashLength * 2);
}
