#include "camera.h"

Camera::Camera(QVector3D rootPos, QVector3D targetPos)
{
    _name = "camera";
    _rootPos = rootPos;
    _targetPos = targetPos;
    _up = QVector3D(0,1,0);

    //_mTransform.setToIdentity();
    //_mTransform.lookAt(_rootPos, _targetPos, _up);
    _mModel.setToIdentity();
    _mModel.lookAt( _rootPos, _targetPos, _up);
}

void Camera::updateMatrix()
{
    QMatrix4x4 camMatrix;

    camMatrix.lookAt( _rootPos, _targetPos, _up);

    _mModel = camMatrix;
}

QMatrix4x4 Camera::getMatrix()
{
    return _mModel;
}

void Camera::rotateCam(QPoint mouseDelta)
{
    //qDebug() << mouseDelta;
    QVector3D pitchAxis;
    QVector3D upVector = QVector3D(0,1,0);

    QVector3D targetToRoot = _rootPos - _targetPos;

    QMatrix4x4 mRot;
    mRot.setToIdentity();
    mRot.rotate( -1 * mouseDelta.x()/5.0, upVector );
    targetToRoot = mRot.map(targetToRoot);
    _rootPos = _targetPos + targetToRoot;
    //_rootPos.setX(_rootPos.x()+0.01);

    float vecLength = targetToRoot.length();

    targetToRoot.normalize();
    pitchAxis = QVector3D::crossProduct( targetToRoot, upVector);

    mRot.setToIdentity();
    mRot.rotate( mouseDelta.y()/5.0, pitchAxis);
    targetToRoot *= vecLength;
    targetToRoot = mRot.map(targetToRoot);
    _rootPos = _targetPos + targetToRoot;

    updateMatrix();
    //_rootPos = _targetPos + targetToRoot;
}

void Camera::panCam(QPoint mouseDelta)
{
    float sensitivity = 0.001;
    QVector3D upVec = QVector3D(0,1,0);
    QVector3D root_to_target = _targetPos - _rootPos;

    sensitivity *= root_to_target.length();

    root_to_target.normalize();

    QVector3D goToLeftVec = QVector3D::crossProduct( root_to_target, upVec );
    QVector3D goToUpVec = QVector3D::crossProduct( root_to_target, goToLeftVec );

    _targetPos += ( -sensitivity * mouseDelta.x() ) * goToLeftVec;
    _targetPos += ( -sensitivity * mouseDelta.y() ) * goToUpVec;

    _rootPos   += ( -sensitivity * mouseDelta.x() ) * goToLeftVec;
    _rootPos   += ( -sensitivity * mouseDelta.y() ) * goToUpVec;

    updateMatrix();
}

void Camera::zoomCam(QPoint mouseDelta)
{
    float sensitivity = 0.005;
    //QVector3D upVec = QVector3D(0,1,0);
    QVector3D root_to_target = _targetPos - _rootPos;

    sensitivity *= root_to_target.length();

    root_to_target.normalize();

    //_targetPos += ( sensitivity * mouseDelta.x() ) * root_to_target;

    _rootPos   += ( sensitivity * mouseDelta.x() ) * root_to_target;

    QVector3D new_root_to_target = _targetPos - _rootPos;

    //new_root_to_target.normalize();

    if ( new_root_to_target.length() < 2.5 )
    {
        _rootPos = _targetPos + ( -2.501 * root_to_target);
    }

    updateMatrix();

}
