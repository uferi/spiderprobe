#include "grid.h"
#include "math.h"

#include "scenemanager.h"

#include <QVector4D>
#include <QDebug>

Grid::Grid(int size, int division, int fadeSize)
{
    _name = "grid";
    _size = size;
    _division = division;
    _fadeSize = fadeSize;
    position_attrib_index   = 0;
    opacity_attrib_index    = 1;
    alignment_attrib_index  = 2;

    int numOfLines = floor( _size / _division );

    qDebug() << "num of lines: " << numOfLines;

    float fsize = (float)_size;
    float pos = -( (numOfLines*_division) /2.0f)+0.5*_division;
    float stepSize = fsize/_division;

    for(int i=0; i<numOfLines; i++)
    {
        // X axis lines
        //      fade in
        addPointToData( QVector4D( -(fsize/2)            , 0, pos, 0), 0 );
        addPointToData( QVector4D( -(fsize/2) + _fadeSize, 0, pos, 1), 0 );
        //      full opacity
        addPointToData( QVector4D( -(fsize/2) + _fadeSize, 0, pos, 1), 0 );
        addPointToData( QVector4D(  (fsize/2) - _fadeSize, 0, pos, 1), 0 );
        //      fade out
        addPointToData( QVector4D(   fsize/2  - _fadeSize, 0, pos, 1), 0 );
        addPointToData( QVector4D(  (fsize/2)            , 0, pos, 0), 0 );



        // Z axis lines
        //      fade in
        addPointToData( QVector4D( pos, 0, -(fsize/2)            , 0), 1 );
        addPointToData( QVector4D( pos, 0, -(fsize/2) + _fadeSize, 1), 1 );
        //      full opacity
        addPointToData( QVector4D( pos, 0, -(fsize/2) + _fadeSize, 1), 1 );
        addPointToData( QVector4D( pos, 0,  (fsize/2) - _fadeSize, 1), 1 );
        //      fade out
        addPointToData( QVector4D( pos, 0,  (fsize/2) - _fadeSize, 1), 1 );
        addPointToData( QVector4D( pos, 0,  (fsize/2)            , 0), 1 );

        pos += _division;

        //qDebug() << i;
    }

}

void Grid::initObject()
{
    //qDebug() << "the grid creates itself";

    SceneManager& scnMng = SceneManager::instance();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();
    glFunc->glGenBuffers(1, &_VBO);
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glBufferData(GL_ARRAY_BUFFER, _data.size()*4, _data.data(), GL_DYNAMIC_DRAW);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)12);

    _shaderProgram = scnMng.getShader(ShaderType::GRID);
    //qDebug() << "on grid the shaderProgram id:" << _shaderProgram;

    for(int i=0; i<_child.length(); i++)
    {
        _child[i]->initObject();
    }
}

void Grid::paintObject(QMatrix4x4 parentMatrix)
{
    //qDebug() << "the grid draws itself";
    SceneManager& scnMng = SceneManager::instance();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();

    glFunc->glUseProgram(_shaderProgram);

    // bind VBO first !!!
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)12);
    glFunc->glEnableVertexAttribArray(0);
    glFunc->glEnableVertexAttribArray(1);
    glFunc->glDisableVertexAttribArray(1);

    QMatrix4x4 modelMatrix;
    QMatrix4x4 projMatrix = scnMng.getProjMatrix();

    unsigned int modelMatrixLoc;
    unsigned int camMatrixLoc;
    unsigned int projMatrixLoc;

    modelMatrix.setToIdentity();
    modelMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"modelMatrix");
    camMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"camMatrix");
    projMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"projMatrix");

    Camera* renderCam = scnMng.getRenderCamera();
    QMatrix4x4 camMatrix;
    camMatrix = renderCam->getMatrix();

    glFunc->glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, (GLfloat*)&modelMatrix );
    glFunc->glUniformMatrix4fv(camMatrixLoc, 1, GL_FALSE, (GLfloat*)&camMatrix );
    glFunc->glUniformMatrix4fv(projMatrixLoc, 1, GL_FALSE, (GLfloat*)&projMatrix);

    glFunc->glLineWidth(1);

    //glDrawArrays(GL_TRIANGLES, 0, 3);
    glFunc->glDrawArrays(GL_LINES, 0, _data.size()/5);
    //glPointSize(5.0);
    //glDrawArrays(GL_POINTS, 0, scnMng.nodeList[0]->_data.size()/5);
}


void Grid::addPointToData(QVector4D point , float alignment)
{
    _data.push_back( point.x() ); // X
    _data.push_back( point.y() ); // Y
    _data.push_back( point.z() ); // Z
    _data.push_back( point.w() ); // opacity value
    _data.push_back( alignment ); // 0 -> paralel o the X axis ; 1 -> paralel to the Z axis
}
