#ifndef DISTANCEGAUGE_H
#define DISTANCEGAUGE_H

#include "general/object3d.h"


class DistanceGauge : public Object3D
{
public:
    DistanceGauge(float radius);
    DistanceGauge(float radius, QVector3D color);

    void initObject() override;
    void paintObject(QMatrix4x4 parentMatrix) override;
    void setColor(QVector3D color);
    void setRadius(float radius);

private:
    bool _initialised;
    float _radius;
    float _innerRadius;
    float _crossLineLength;
    QVector3D _color;
    int _circlePointAmount;

    void addPoint(QVector3D pos, QVector3D col);
    void buildData();
};

#endif // DISTANCEGAUGE_H
