#ifndef CAMERA_H
#define CAMERA_H

#include "general/entity.h"
//#include "general/node3d.h"
#include "general/object3d.h"

#include <string>

class Camera : public Entity, public Object3D
{
public:
    Camera(QVector3D rootPos, QVector3D targetPos);
    void updateMatrix();
    QMatrix4x4 getMatrix();
    void rotateCam(QPoint mouseDelta);
    void panCam(QPoint mouseDelta);
    void zoomCam(QPoint mouseDelta);

private:
    QVector3D _rootPos;
    QVector3D _targetPos;
    QVector3D _up;

    //QMatrix4x4 _mTransform;
};

#endif // CAMERA_H
