#include "backgroundgradient.h"

#include "scenemanager.h"


BackgroundGradient::BackgroundGradient()
{
    QVector3D colorTop    = QVector3D( 129/255.0, 148/255.0, 169/255.0);    // 129, 148, 169
    QVector3D colorBottom = QVector3D( (52/255.0)*0.8, (60/255.0)*0.8, (69/255.0)*0.8);       //  52,  60,  69
    float depth = 1000000;

    addPoint(QVector3D( -depth, -depth , -depth), colorBottom);
    addPoint(QVector3D(  depth, -depth , -depth), colorBottom);
    addPoint(QVector3D( -depth,  depth , -depth), colorTop);

    addPoint(QVector3D(  depth, -depth , -depth), colorBottom);
    addPoint(QVector3D(  depth,  depth , -depth), colorTop);
    addPoint(QVector3D( -depth,  depth , -depth), colorTop);

}

void BackgroundGradient::initObject()
{
    SceneManager& scnMng = SceneManager::instance();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();
    glFunc->glGenBuffers(1, &_VBO);
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glBufferData(GL_ARRAY_BUFFER, _data.size()*4, _data.data(), GL_STATIC_DRAW);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)12);

    _shaderProgram = scnMng.getShader(ShaderType::VERTEX_COLOR);

    for(int i=0; i<_child.length(); i++)
    {
        _child[i]->initObject();
    }
}

void BackgroundGradient::paintObject(QMatrix4x4 parentMatrix)
{
    //qDebug() << "the background gradient draws itself";
    SceneManager& scnMng = SceneManager::instance();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();

    glFunc->glUseProgram(_shaderProgram);

    // bind VBO first !!!
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)12);
    glFunc->glEnableVertexAttribArray(0);
    glFunc->glEnableVertexAttribArray(1);

    QMatrix4x4 modelMatrix, noCameraTransform;
    QMatrix4x4 projMatrix = scnMng.getProjMatrix();

    unsigned int modelMatrixLoc;
    unsigned int camMatrixLoc;
    unsigned int projMatrixLoc;

    modelMatrix.setToIdentity();
    noCameraTransform.setToIdentity();

    modelMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"modelMatrix");
    camMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"camMatrix");
    projMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"projMatrix");

    Camera* renderCamera = scnMng.getRenderCamera();
    QMatrix4x4 camMatrix;
    camMatrix = renderCamera->getMatrix();

    glFunc->glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, (GLfloat*)&modelMatrix );
    glFunc->glUniformMatrix4fv(camMatrixLoc, 1, GL_FALSE, (GLfloat*)&noCameraTransform );
    glFunc->glUniformMatrix4fv(projMatrixLoc, 1, GL_FALSE, (GLfloat*)&projMatrix);

    //glFunc->glLineWidth(1);

    //glDrawArrays(GL_TRIANGLES, 0, 3);
    glFunc->glDrawArrays(GL_TRIANGLES, 0, _data.size()/6);
    //glPointSize(5.0);
    //glDrawArrays(GL_POINTS, 0, scnMng.nodeList[0]->_data.size()/5);

}

void BackgroundGradient::addPoint(QVector3D pos, QVector3D col)
{
    _data.push_back( pos.x()/20 );
    _data.push_back( pos.y()/20 );
    _data.push_back( pos.z()/20 );

    _data.push_back( col.x() );
    _data.push_back( col.y() );
    _data.push_back( col.z() );
}
