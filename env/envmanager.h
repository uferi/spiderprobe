#ifndef ENVMANAGER_H
#define ENVMANAGER_H

#include <QVector>

#include "env/grid.h"
#include "env/origingnomon.h"
#include "env/backgroundgradient.h"
#include "env/camera.h"
#include "general/object3d.h"

class EnvManager
{
public:
    EnvManager(QVector<Object3D*> &nodeList, Object3D* root);
    ~EnvManager();
    Camera* getCamera();

private:
    BackgroundGradient *bgGradient;
    Grid     *grid;
    OriginGnomon *originGnomon;
    Camera *camera;
};

#endif // ENVMANAGER_H
