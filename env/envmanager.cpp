#include "envmanager.h"
#include "scenemanager.h"
#include <QDebug>

EnvManager::EnvManager(QVector<Object3D*> &nodeList, Object3D *root)
{

    bgGradient = new BackgroundGradient();
    bgGradient->setObjectName("bgGradient");
    bgGradient->setParentObject(root);
    nodeList.append(bgGradient);

    grid = new Grid(320, 20, 10);
    grid->setObjectName("grid");
    grid->setParentObject(root);
    nodeList.append(grid);

    originGnomon = new OriginGnomon(10);
    originGnomon->setObjectName("originGnomon");
    originGnomon->setParentObject(root);
    nodeList.append(originGnomon);

    //QVector3D camRoot = QVector3D(10,10,10);
    //QVector3D camTarget = QVector3D(0,0,0);

    //camera = new Camera(camRoot, camTarget);
    //nodeList.append(camera);
}

EnvManager::~EnvManager()
{
    //delete grid;
    //delete camera;
}

Camera* EnvManager::getCamera()
{
    return camera;
}
