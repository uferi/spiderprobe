#ifndef LOCATOR_H
#define LOCATOR_H

#include "general/object3d.h"

class Locator : public Object3D
{
public:
    Locator(float size);
    Locator(float size, QVector3D color);

    void initObject() override;
    void paintObject(QMatrix4x4 parentMatrix) override;
    void setColor(QVector3D color);

private:
    float _size;
    QVector3D _color;

    void addPoint(QVector3D pos, QVector3D col);

};

#endif // LOCATOR_H
