#ifndef BEZIERCURVE_H
#define BEZIERCURVE_H

#include "general/object3d.h"

class BezierCurve : public Object3D
{
public:
    BezierCurve(QVector3D pt0, QVector3D pt1, QVector3D pt2, QVector3D pt3);

    void initObject() override;
    void paintObject(QMatrix4x4 parentMatrix) override;

    //void updateCurve()

private:
    bool _initialised;
    QVector3D _point[4];
    QVector3D _sideVector[3];

    QVector3D _color;
    int _resolution;

    void addPoint(QVector3D pos, QVector3D col);
    void buildData();
};

#endif // BEZIERCURVE_H
