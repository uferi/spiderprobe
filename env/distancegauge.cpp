#include "distancegauge.h"

#include "scenemanager.h"

DistanceGauge::DistanceGauge(float radius)
{
    _initialised = false;
    _radius = radius;
    _color = QVector3D(0.3,0.5,0.3);

    _circlePointAmount = 360;
    _innerRadius = 3;
    _crossLineLength = 5;

    buildData();
}

DistanceGauge::DistanceGauge(float radius, QVector3D color)
{
    _initialised = false;
    _radius = radius;
    _color = color;

    _circlePointAmount = 360;
    _innerRadius = 3;
    _crossLineLength = 5;

    buildData();
}

void DistanceGauge::initObject()
{
    SceneManager& scnMng = SceneManager::instance();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();
    glFunc->glGenBuffers(1, &_VBO);
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glBufferData(GL_ARRAY_BUFFER, _data.size()*4, _data.data(), GL_STATIC_DRAW);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)12);

    _shaderProgram = scnMng.getShader(ShaderType::VERTEX_COLOR);

    for(int i=0; i<_child.length(); i++)
    {
        _child[i]->initObject();
    }
    _initialised = true;
}

void DistanceGauge::paintObject(QMatrix4x4 parentMatrix)
{
    //qDebug() << "the distance gauge draws itself";

    SceneManager& scnMng = SceneManager::instance();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();

    glFunc->glUseProgram(_shaderProgram);

    // bind VBO first !!!
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)12);
    glFunc->glEnableVertexAttribArray(0);
    glFunc->glEnableVertexAttribArray(1);

    QMatrix4x4 modelMatrix;
    QMatrix4x4 projMatrix = scnMng.getProjMatrix();

    unsigned int modelMatrixLoc;
    unsigned int camMatrixLoc;
    unsigned int projMatrixLoc;

    modelMatrix =  parentMatrix * _mModel;
    modelMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"modelMatrix");
    camMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"camMatrix");
    projMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"projMatrix");

    Camera* renderCamera = scnMng.getRenderCamera();
    QMatrix4x4 camMatrix;
    camMatrix = renderCamera->getMatrix();

    glFunc->glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, (GLfloat*)&modelMatrix );
    glFunc->glUniformMatrix4fv(camMatrixLoc, 1, GL_FALSE, (GLfloat*)&camMatrix );
    glFunc->glUniformMatrix4fv(projMatrixLoc, 1, GL_FALSE, (GLfloat*)&projMatrix);

    glFunc->glLineWidth(1.5);
    glFunc->glDrawArrays(GL_LINES, 0, _data.size()/6);

    QMatrix4x4 localMatrix;
    localMatrix = parentMatrix * _mModel;
    //localMatrix.translate(0,0,_length);       // only bone has this offset
    for(int i=0; i<_child.length(); i++)
    {
        _child[i]->paintObject(localMatrix);
    }
}

void DistanceGauge::setColor(QVector3D color)
{
    _color = color;

    _data.clear();

    buildData();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();
    //glFunc->glGenBuffers(1, &_VBO);
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glBufferData(GL_ARRAY_BUFFER, _data.size()*4, _data.data(), GL_STATIC_DRAW);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)12);
}

void DistanceGauge::setRadius(float radius)
{
    _radius = radius;

    _data.clear();

    buildData();

    if(_initialised)
    {
    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();
    //glFunc->glGenBuffers(1, &_VBO);
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glBufferData(GL_ARRAY_BUFFER, _data.size()*4, _data.data(), GL_STATIC_DRAW);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)12);
    }
}

void DistanceGauge::addPoint(QVector3D pos, QVector3D col)
{
    _data.push_back( pos.x() );
    _data.push_back( pos.y() );
    _data.push_back( pos.z() );

    _data.push_back( col.x() );
    _data.push_back( col.y() );
    _data.push_back( col.z() );
}

void DistanceGauge::buildData()
{
    QVector3D circleFirstPoint = QVector3D(_radius,0,0);
    QVector3D innerFirstPoint = QVector3D(_innerRadius,0,0);
    QMatrix4x4 rotMatrix;
    rotMatrix.setToIdentity();
    rotMatrix.rotate(_circlePointAmount/360,0,1,0);
    QVector3D circleSecondPoint = rotMatrix.map(circleFirstPoint);
    QVector3D innerSecondPoint = rotMatrix.map(innerFirstPoint);

    for( int i=0; i<_circlePointAmount; i++)
    {
        addPoint(circleFirstPoint, _color);
        addPoint(circleSecondPoint, _color);

        addPoint(innerFirstPoint, _color);
        addPoint(innerSecondPoint, _color);

        circleFirstPoint = rotMatrix.map(circleFirstPoint);
        circleSecondPoint = rotMatrix.map(circleSecondPoint);

        innerFirstPoint = rotMatrix.map(innerFirstPoint);
        innerSecondPoint = rotMatrix.map(innerSecondPoint);
    }

    addPoint(QVector3D(-_innerRadius, 0, 0),_color);
    addPoint(QVector3D((-_innerRadius-_crossLineLength), 0, 0),_color);

    addPoint(QVector3D(_innerRadius, 0, 0),_color);
    addPoint(QVector3D(_innerRadius+_crossLineLength, 0, 0),_color);

    addPoint(QVector3D( 0, 0,-_innerRadius),_color);
    addPoint(QVector3D( 0, 0,-_innerRadius-_crossLineLength),_color);

    addPoint(QVector3D( 0, 0, _innerRadius),_color);
    addPoint(QVector3D( 0, 0, _innerRadius+_crossLineLength),_color);
}
