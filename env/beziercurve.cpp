#include "beziercurve.h"

#include "scenemanager.h"

BezierCurve::BezierCurve(QVector3D pt0, QVector3D pt1, QVector3D pt2, QVector3D pt3)
{
    _initialised = false;

    _color = QVector3D(0.3,0.5,0.9);
    _resolution = 32;

    _point[0] = pt0;
    _point[1] = pt1;
    _point[2] = pt2;
    _point[3] = pt3;

    _sideVector[0] = _point[1] - _point[0];
    _sideVector[1] = _point[2] - _point[1];
    _sideVector[2] = _point[3] - _point[2];

    buildData();

}

void BezierCurve::initObject()
{
    SceneManager& scnMng = SceneManager::instance();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();
    glFunc->glGenBuffers(1, &_VBO);
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glBufferData(GL_ARRAY_BUFFER, _data.size()*4, _data.data(), GL_STATIC_DRAW);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)12);

    _shaderProgram = scnMng.getShader(ShaderType::VERTEX_COLOR);

    for(int i=0; i<_child.length(); i++)
    {
        _child[i]->initObject();
    }
    _initialised = true;
}

void BezierCurve::paintObject(QMatrix4x4 parentMatrix)
{
    //qDebug() << "the dashed line draws itself";

    SceneManager& scnMng = SceneManager::instance();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();

    glFunc->glUseProgram(_shaderProgram);

    // bind VBO first !!!
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)12);
    glFunc->glEnableVertexAttribArray(0);
    glFunc->glEnableVertexAttribArray(1);

    QMatrix4x4 modelMatrix;
    QMatrix4x4 projMatrix = scnMng.getProjMatrix();

    unsigned int modelMatrixLoc;
    unsigned int camMatrixLoc;
    unsigned int projMatrixLoc;

    modelMatrix =  parentMatrix * _mModel;
    modelMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"modelMatrix");
    camMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"camMatrix");
    projMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"projMatrix");

    Camera* renderCamera = scnMng.getRenderCamera();
    QMatrix4x4 camMatrix;
    camMatrix = renderCamera->getMatrix();

    glFunc->glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, (GLfloat*)&modelMatrix );
    glFunc->glUniformMatrix4fv(camMatrixLoc, 1, GL_FALSE, (GLfloat*)&camMatrix );
    glFunc->glUniformMatrix4fv(projMatrixLoc, 1, GL_FALSE, (GLfloat*)&projMatrix);

    glFunc->glLineWidth(1.5);
    glFunc->glDrawArrays(GL_LINES, 0, _data.size()/6);

    QMatrix4x4 localMatrix;
    localMatrix = parentMatrix * _mModel;
    //localMatrix.translate(0,0,_length);       // only bone has this offset
    for(int i=0; i<_child.length(); i++)
    {
        _child[i]->paintObject(localMatrix);
    }
}

void BezierCurve::addPoint(QVector3D pos, QVector3D col)
{
    _data.push_back( pos.x() );
    _data.push_back( pos.y() );
    _data.push_back( pos.z() );

    _data.push_back( col.x() );
    _data.push_back( col.y() );
    _data.push_back( col.z() );
}

void BezierCurve::buildData()
{
    QVector3D previousPoint = _point[0];

    for(int i=1; i<=_resolution; i++)
    {
        float ratio = (float)i/(float)_resolution;
        QVector3D level_1_pt0 = _point[0] + _sideVector[0] * ratio;
        QVector3D level_1_pt1 = _point[1] + _sideVector[1] * ratio;
        QVector3D level_1_pt2 = _point[2] + _sideVector[2] * ratio;
        QVector3D level_1_sideVector0 = level_1_pt1 - level_1_pt0;
        QVector3D level_1_sideVector1 = level_1_pt2 - level_1_pt1;

        QVector3D level_2_pt0 = level_1_pt0 + level_1_sideVector0 * ratio;
        QVector3D level_2_pt1 = level_1_pt1 + level_1_sideVector1 * ratio;
        QVector3D level_2_sideVector0 = level_2_pt1 - level_2_pt0;

        QVector3D actualPoint = level_2_pt0 + level_2_sideVector0 * ratio;

        addPoint( previousPoint, _color);
        addPoint(   actualPoint, _color);

        previousPoint = actualPoint;
    }
}
