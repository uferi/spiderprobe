#include "mainwindow.h"
#include "scenemanager.h"
#include "glwidget.h"

#include <QApplication>
#include <QWidget>
#include <QSurfaceFormat>
#include <QOpenGLContext>
#include <QOpenGLFunctions>

#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QSurfaceFormat fmt;
    fmt.setDepthBufferSize(24);
    fmt.setSamples(4);
    fmt.setVersion(3, 2);
    fmt.setProfile(QSurfaceFormat::CoreProfile);

    //SceneManager& scnMng = SceneManager::instance();

    MainWindow w;
    //w.connectValues(&scnMng);
    w.show();

    return a.exec();
}
