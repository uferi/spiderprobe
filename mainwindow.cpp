#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "scenemanager.h"

#include <QSlider>
#include <QSpinBox>

//class openglWidget;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //ui->openglWidget->update();

    //ui->openglWidget->setFlag(QGrph)
    //ui->openglWidget->setFocus();

    connect(ui->segASpinBox, SIGNAL(valueChanged(int)), ui->segASlider, SLOT(setValue(int)));
    connect(ui->segASlider, SIGNAL(valueChanged(int)), ui->segASpinBox, SLOT(setValue(int)));
    connect(ui->segBSpinBox, SIGNAL(valueChanged(int)), ui->segBSlider, SLOT(setValue(int)));
    connect(ui->segBSlider, SIGNAL(valueChanged(int)), ui->segBSpinBox, SLOT(setValue(int)));
    connect(ui->segCSpinBox, SIGNAL(valueChanged(int)), ui->segCSlider, SLOT(setValue(int)));
    connect(ui->segCSlider, SIGNAL(valueChanged(int)), ui->segCSpinBox, SLOT(setValue(int)));

    connect(ui->rootXSpinBox, SIGNAL(valueChanged(int)), ui->rootXSlider, SLOT(setValue(int)));
    connect(ui->rootXSlider, SIGNAL(valueChanged(int)), ui->rootXSpinBox, SLOT(setValue(int)));
    connect(ui->rootZSpinBox, SIGNAL(valueChanged(int)), ui->rootZSlider, SLOT(setValue(int)));
    connect(ui->rootZSlider, SIGNAL(valueChanged(int)), ui->rootZSpinBox, SLOT(setValue(int)));

    connect(ui->restAngleSpinBox, SIGNAL(valueChanged(int)), ui->restAngleSlider, SLOT(setValue(int)));
    connect(ui->restAngleSlider, SIGNAL(valueChanged(int)), ui->restAngleSpinBox, SLOT(setValue(int)));

    connect(ui->bodyHeightCSpinBox, SIGNAL(valueChanged(int)), ui->bodyHeightSlider, SLOT(setValue(int)));
    connect(ui->bodyHeightSlider, SIGNAL(valueChanged(int)), ui->bodyHeightCSpinBox, SLOT(setValue(int)));

    ui->segASpinBox->setValue(30);
    ui->segBSpinBox->setValue(70);
    ui->segCSpinBox->setValue(70);

    ui->rootXSpinBox->setValue(25);
    ui->rootZSpinBox->setValue(40);
    ui->restAngleSpinBox->setValue(15);
    ui->bodyHeightCSpinBox->setValue(40);

    SceneManager& scnMng = SceneManager::instance();

    connect(ui->segASpinBox, SIGNAL(valueChanged(int)), &scnMng, SLOT(setSegALength(int)));
    connect(ui->segBSpinBox, SIGNAL(valueChanged(int)), &scnMng, SLOT(setSegBLength(int)));
    connect(ui->segCSpinBox, SIGNAL(valueChanged(int)), &scnMng, SLOT(setSegCLength(int)));
    connect(ui->rootXSpinBox, SIGNAL(valueChanged(int)), &scnMng, SLOT(setRootXPos(int)));
    connect(ui->rootZSpinBox, SIGNAL(valueChanged(int)), &scnMng, SLOT(setRootZPos(int)));
    connect(ui->restAngleSpinBox, SIGNAL(valueChanged(int)), &scnMng, SLOT(setRootRestAngle(int)));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::connectValues(SceneManager *scnMng)
{
    connect(ui->segASpinBox, SIGNAL(valueChanged(int)), scnMng, SLOT(setSegALength(int)));
    connect(ui->segBSpinBox, SIGNAL(valueChanged(int)), scnMng, SLOT(setSegBLength(int)));
    connect(ui->segCSpinBox, SIGNAL(valueChanged(int)), scnMng, SLOT(setSegCLength(int)));
    connect(ui->rootXSpinBox, SIGNAL(valueChanged(int)), scnMng, SLOT(setRootXPos(int)));
    connect(ui->rootZSpinBox, SIGNAL(valueChanged(int)), scnMng, SLOT(setRootZPos(int)));
    connect(ui->restAngleSpinBox, SIGNAL(valueChanged(int)), scnMng, SLOT(setRootRestAngle(int)));
    connect(ui->bodyHeightCSpinBox, SIGNAL(valueChanged(int)), scnMng, SLOT(setBodyHeight(int)));
}
