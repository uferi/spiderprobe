#include "glwidget.h"
#include "general/object3d.h"
#include "scenemanager.h"


GLWidget::GLWidget(QWidget *parent)
    : QOpenGLWidget(parent)
{
    isCameraMovable = false;

    QSurfaceFormat fmt;
    fmt.setDepthBufferSize(24);
    fmt.setSamples(4);
    fmt.setVersion(3, 2);
    //fmt.setProfile(QSurfaceFormat::CoreProfile);
    fmt.setProfile(QSurfaceFormat::CompatibilityProfile);

    setFormat(fmt);

    QSurfaceFormat fmt2 = format();
    qDebug() << "major: " << fmt2.majorVersion();
    qDebug() << "minor: " << fmt2.minorVersion();
    qDebug() << "depth buffer size" << fmt2.depthBufferSize();

    //set the default opengl update
    QTimer *displayTimer = new QTimer;
    connect( displayTimer, SIGNAL( timeout() ), this, SLOT( update() ) );
    displayTimer->start(1000/60);

}

void GLWidget::initializeGL()
{   
    SceneManager& scnMng = SceneManager::instance();

    qDebug() << "initializeGL";

    initializeOpenGLFunctions();

    scnMng.buildShaders();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glEnable(GL_DEPTH_TEST);

    glClearColor(0.2, 0.2, 0.2, 0.0);

    scnMng.initScene();

}

void GLWidget::paintGL()
{
    // ??????????   How to get the object list that needs to be rendered in here?
    //              The main thing is: this is the only pleace (and initializeGL, resizeGL_ where I have control over the actual context
    //              Or: how can I pass this context over to an other class to use it and draw with it?

    //qDebug() << "Yo - paintGL";

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //glEnable(GL_DEPTH_TEST);
    //glEnable(GL_CULL_FACE);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    SceneManager& scnMng = SceneManager::instance();

    //glClearColor(0.9, 0.4, 0.4, 1.0);
    //glBlendColor(0.0, 0.0, 0.5, 1.0);

    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    //glEnable(GL_DEPTH_TEST | GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //glUseProgram(shaderProgram);

    //qDebug() << "nodeList length: "<< scnMng.nodeList.length();

    scnMng.renderScene();

    //for(int i=0; i<scnMng.nodeList.length(); i++)
    //{
    //    scnMng.nodeList[i]->paintObject(projMatrix);
    //}


}

void GLWidget::resizeGL(int w, int h)
{
    SceneManager& scnMng = SceneManager::instance();

    projMatrix.setToIdentity();
    projMatrix.perspective(35.0f,(float)w/h,1.0,1000000);

    scnMng.setProjMatrix(projMatrix);
    //projMatrix.ortho(-1,1,-1,1,-100,100);
    //qDebug() << w << " " << h;
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    _lastPos = event->pos();
    //qDebug() << _lastPos;
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    SceneManager& scnMng = SceneManager::instance();
    Camera* renderCam = scnMng.getRenderCamera();

    //qDebug() << event->x();
    int dx = event->x() - _lastPos.x();
    int dy = event->y() - _lastPos.y();
    _lastPos = event->pos();

    if ( event->buttons() & Qt::LeftButton )
    {
        renderCam->rotateCam( QPoint(dx,dy) );
    }

    if ( event->buttons() & Qt::MiddleButton)
    {
        renderCam->panCam( QPoint(dx,dy) );
    }

    if ( event->buttons() & Qt::RightButton)
    {
        renderCam->zoomCam( QPoint(dx,dy) );
    }

    update();
    //
    //if (event->buttons() & Qt::LeftButton) {
    //    setXRotation(m_xRot - 8 * dy);
    //    setYRotation(m_yRot - 8 * dx);
    //} else if (event->buttons() & Qt::RightButton) {
    //    setXRotation(m_xRot + 8 * dy);
    //    setZRotation(m_zRot + 8 * dx);
    //}
    //m_lastPos = event->pos();
}

void GLWidget::keyPressEvent(QKeyEvent *keyEvent)
{
    //qDebug() << keyEvent->key();

    SceneManager& scnMng = SceneManager::instance();

    if ( keyEvent->key() == Qt::Key_Control )
    {
        qDebug() << "camera movable";
        isCameraMovable = true;
    }
    if ( keyEvent->key() == Qt::Key_Space )
    {
        //qDebug() << "Space Pressed";
        scnMng.toggleRunning();
    }
}

void GLWidget::keyReleaseEvent(QKeyEvent *keyEvent)
{
    if ( keyEvent->key() == Qt::Key_Control )
    {
        qDebug() << "camera NOT movable";
        isCameraMovable = false;
    }
}
