#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include <QObject>
#include <QTimer>
#include <QVector>
#include <QOpenGLWidget>
#include <QOpenGLContext>
#include <QOpenGLFunctions>
#include <QTime>
#include <QtGamepad/QGamepad>

#include "env/envmanager.h"
#include "env/cameramanager.h"
#include "spider/spider.h"
#include "general/object3d.h"
#include "general/shadermanager.h"

#include "spider/bone.h"
#include "env/locator.h"
#include "env/dashedline.h"
#include "env/beziercurve.h"

class SceneManager : public QObject
{
    Q_OBJECT
public:
    explicit SceneManager(QObject *parent = nullptr);
    static SceneManager& instance();
    ~SceneManager();
    EnvManager* getEnv();
    Spider* getSpider();
    void buildShaders();
    int getShader(int shader);
    Camera* getRenderCamera();
    Object3D* getObject(QString name);
    void setProjMatrix(QMatrix4x4 matrix);
    QMatrix4x4 getProjMatrix();
    int getMasterFps();
    void renderScene();
    void initScene();
    void toggleRunning();
    void testDraw();

    QVector3D getTranslate(QMatrix4x4 matrix);

    QVector<Object3D*> nodeList;
    QOpenGLContext *context;
    QOpenGLFunctions *f;
    QOpenGLWidget *glw;
    QTimer masterTimer;

signals:

public slots:
    void masterUpdate();
    void setSegALength(int value);
    void setSegBLength(int value);
    void setSegCLength(int value);
    void setRootXPos(int value);
    void setRootZPos(int value);
    void setRootRestAngle(int value);
    void setBodyHeight(int value);

    void rotateSpiderX(double angle);
    void rotateSpiderY(double angle);
    void moveSpiderX(double value);
    void moveSpiderY(double value);
    void moveSpiderZ(double value);
    void resetBody(bool button);


private:
    //SceneManager(QObject *parent = nullptr);
    //static SceneManager *instance_;

    QGamepad *_gamepad;
    double axisLeftX;
    double axisLeftY;
    QVector3D spiderMoveVector;

    QMatrix4x4 projMatrix;

    Object3D *root;

    ShaderManager *shaderManager;
    EnvManager *envManager;
    CameraManager *cameraManager;

    Spider *spider;

    Bone *bone;
    Bone *bone2;
    Bone *bone3;
    Bone *bone4;

    Bone *ikbone1;
    Bone *ikbone2;
    Bone *ikbone3;
    Locator *iklocend;
    Locator *ikloc1;
    Locator *ikloc2;
    Locator *ikloc3;

    Locator *loc1;

    QTime fpsTime;
    QTime time;
    float fpsCount;

    int masterFps;
    bool _running;

};

#endif // SCENEMANAGER_H
