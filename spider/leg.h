#ifndef LEG_H
#define LEG_H

#include "general/object3d.h"
#include "spider/bone.h"
//#include "spider/segment.h"

class Leg
{
public:
    Leg(Object3D* parent, int segALength, int segBLength, int segCLength);
    void resizeSegA(float length);
    void resizeSegB(float length);
    void resizeSegC(float length);
    void setLegColor(QVector3D color);
    void setMatrix(int segmentNum, QMatrix4x4 matrix);
    void setRotation(int segmentNum, float angle);


private:
    //Bone *segA;
    //Bone *segB;
    //Bone *segC;
    Bone *_segment[3];

    //Segment segment[3];
};

#endif // LEG_H
