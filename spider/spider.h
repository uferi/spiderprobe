﻿#ifndef SPIDER_H
#define SPIDER_H

#include <QVector3D>
#include <QVector2D>
#include <QString>
#include <QTime>
#include <QTimer>

#include "spider/leg.h"
#include "general/object3d.h"

#include "env/locator.h"
#include "env/distancegauge.h"
#include "spider/leg.h"
#include "spider/bone.h"
#include "env/dashedline.h"
#include "spider/stepcurve.h"

class Spider : public Object3D
{
public:
    Spider(Object3D* root);

    void setSegALength(float value);
    void setSegBLength(float value);
    void setSegCLength(float value);
    void setRootXPos(int value);
    void setRootZPos(int value);
    void setRootRestAngle(int value);
    void setBodyHeight(int value);

    int getSegALength() const;
    int getSegBLength() const;
    int getSegCLength() const;
    int getRootXPos() const;
    int getRootZPos() const;
    int getRootRestAngle() const;
    int getBodyHeight() const;

    void rotate(QVector3D angle);
    void move(QVector3D moveVector);
    void resetBody();
    void toggleTimer();

    QVector3D solveIK();

    QString getName();
    QVector3D getPos(QMatrix4x4 matrix);

private:

    void update_IK_end();
    void update_optimalIkPos();
    void update_stepCurve();
    //void update_optimalRadius();

    int _legAmount;
    float _segLength[3];
    QVector3D _spiderPos;
    QVector3D _spiderAim; // this should be normalized
    QVector3D _legRootPos;
    float _legRestAngle;

    QVector3D _velocity;
    QVector3D _velocityMax;
    QVector3D _angularVelocity;
    QVector3D _angularVelocityMax;
    QVector3D _acceleration;
    QVector3D _accelerationMax;
    QVector3D _angularAcceleration;
    QVector3D _angularAccelerationMax;

    Object3D *_bodyGrp;
    Object3D *_legRootGrp[4];
    Object3D *_legRestAngleGrp[4];
    Leg *_leg[4];
    Locator *_IK_end[4];
    DashedLine *_line_ik_to_optimal[4];

    DistanceGauge *_optimalIkPos[4];
    QMatrix4x4 _mBodyPrev;

    StepCurve *_stepCurve[4];

    QTime _stepTimer;
    int _stepTimerAlreadyElapsed;
    bool  _stepTimerRunning;
    float _stepTempo;
    float _stepPhase;

    Locator *_debugLoc[4];
    Locator *_debugLocB[4];
};

#endif // SPIDER_H
