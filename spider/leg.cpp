#include "leg.h"

Leg::Leg(Object3D *parent, int segALength, int segBLength, int segCLength)
{
    _segment[0] = new Bone( (float)segALength);
    _segment[0]->setObjectName(QString("segA"));
    _segment[0]->setParentObject(parent);
    //_segment[0]->setColor(QVector3D(0.9,0.25,0.1));

    _segment[1] = new Bone( (float)segBLength);
    _segment[1]->setObjectName(QString("segB"));
    _segment[1]->setParentObject(_segment[0]);

    _segment[2] = new Bone( (float)segCLength);
    _segment[2]->setObjectName(QString("segC"));
    _segment[2]->setParentObject(_segment[1]);
}

void Leg::resizeSegA(float length)
{
    _segment[0]->resizeBone(length);
}

void Leg::resizeSegB(float length)
{
    _segment[1]->resizeBone(length);
}

void Leg::resizeSegC(float length)
{
    _segment[2]->resizeBone(length);
}

void Leg::setLegColor(QVector3D color)
{
    //_segment[0]->setColor(color);
    _segment[1]->setColor(color);
    //_segment[2]->setColor(color);
}

void Leg::setMatrix(int _segmentNum, QMatrix4x4 matrix)
{
    _segment[_segmentNum]->_mModel = matrix;
}

void Leg::setRotation(int segmentNum, float angle)
{

    switch (segmentNum) {
    case 0:
        _segment[0]->_mModel.setToIdentity();
        _segment[0]->_mModel.rotate(angle , 0, 1, 0);
        break;
    case 1:
        _segment[1]->_mModel.setToIdentity();
        _segment[1]->_mModel.rotate(angle , 1, 0, 0);
        break;
    case 2:
        _segment[2]->_mModel.setToIdentity();
        _segment[2]->_mModel.rotate(angle , 1, 0, 0);
        break;
    default:
        break;
    }
}
