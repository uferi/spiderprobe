#include "stepcurve.h"

#include "scenemanager.h"

StepCurve::StepCurve(Object3D *root, QVector3D startPos, QVector3D endPos, QVector3D velocity)
{
    _startPos = startPos;
    _endPos = endPos;
    _velocity = velocity;

    QVector3D velDir = _velocity;
    velDir.normalize();
    QVector3D up = QVector3D(0,10,0);

    _curve[0] = new BezierCurve(_startPos, _startPos+(-1*_velocity), _startPos+up+(-1*_velocity),_startPos+up);
    _curve[0]->setParentObject(root);

    _curve[1] = new BezierCurve(_startPos+up,_startPos+up+(+1*_velocity), _endPos +up+(-1*_velocity), _endPos+up );
    _curve[1]->setParentObject(root);

    _curve[2] = new BezierCurve( _endPos+up, _endPos+up+(+1*_velocity), _endPos+(+1*_velocity), _endPos );
    _curve[2]->setParentObject(root);

}

void StepCurve::updateCurve(QVector3D startPos, QVector3D endPos, QVector3D velocity)
{
    //need to be updated on the fly
}
