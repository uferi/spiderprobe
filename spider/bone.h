#ifndef BONE_H
#define BONE_H

#include "general/object3d.h"

class Bone : public Object3D
{
public:
    Bone(float length);
    Bone(float length, QVector3D color);

    void initObject() override;
    void paintObject(QMatrix4x4 parentMatrix) override;
    //void setParentObject(Object3D* parent) override;
    float getLength();
    void resizeBone(float length);
    void setColor(QVector3D color);

private:
    void addPoint(QVector3D pos, QVector3D normal, QVector3D col);
    QVector3D faceNormal( QVector3D p0, QVector3D p1, QVector3D p2 );
    void createGeometryData(QVector3D color);

    float _length;
    float _lengthRatio;
    float _widthRatio;
    float _heightRatio;
    QVector3D _color;
};

#endif // BONE_H
