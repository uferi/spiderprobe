#include "spider.h"

#include <QDebug>

#include <math.h>

#include <scenemanager.h>

Spider::Spider(Object3D *root)
{
    _legAmount = 4;

    _velocity = QVector3D(0,0,0);
    _acceleration = QVector3D(0,0,0);
    _spiderAim = QVector3D(0,0,1);

    _legRootPos = QVector3D(25,40,40);
    _legRestAngle = 15;

    _segLength[0] = 30;
    _segLength[1] = 70;
    _segLength[2] = 70;

    _name = "spider";


    // ---------------------- CREATE BODY ----------------------
    _bodyGrp = new Object3D;
    _bodyGrp->setObjectName(QString("bodyGrp"));
    _bodyGrp->setParentObject(root);
    _bodyGrp->_mModel.translate(0,_legRootPos.y(),0);
    //_body->_mModel.rotate(20,1,0,0);

    Locator *locBodyGrp = new Locator(20,QVector3D(0.7,0.7,0.2));
    locBodyGrp->setObjectName(QString("locBodyGrp"));
    locBodyGrp->setParentObject(_bodyGrp);

    // ---------------------- CREATE LEGS ----------------------
    Locator* locLegRoot[4];

    for(int i=0; i<_legAmount; i++) // --- for the legs
    {
        QString numberStr;
        numberStr.setNum(i);
        // ---- group that has the translation only ----
        _legRootGrp[i] = new Object3D;
        QString name = "legRootGrp";
        name.append(numberStr);
        qDebug() << name;
        _legRootGrp[i]->setObjectName(name);
        _legRootGrp[i]->setParentObject(_bodyGrp);
        _legRootGrp[i]->_mModel.translate( pow(-1,i) * _legRootPos.x(),0, _legRootPos.z() * (-2 * (i/2) +1 ) );

        // ---- group that has the rotation only ----
        _legRestAngleGrp[i] = new Object3D;
        name = "legRestAngleGrp";
        name.append(numberStr);
        _legRestAngleGrp[i]->setObjectName(name);
        _legRestAngleGrp[i]->setParentObject(_legRootGrp[i]);
        QMatrix4x4 tempMatrix;
        tempMatrix.setToIdentity();
        tempMatrix.rotate(_legRestAngle+(i/2)*180, 0, (float)pow(-1,i)*(-2*(i/2)+1), 0);
        _legRestAngleGrp[i]->_mModel = tempMatrix;

        // ---- locator at the root of each leg ----
        locLegRoot[i] = new Locator(10);
        name = "locLegRoot";
        name.append(numberStr);
        locLegRoot[i]->setObjectName(name);
        locLegRoot[i]->setParentObject(_legRestAngleGrp[i]);

        _leg[i] = new Leg(_legRestAngleGrp[i], _segLength[0], _segLength[1], _segLength[2]);

        _IK_end[i] = new Locator( 15, QVector3D(0.6,0.2,0.2));
        _IK_end[i]->setParentObject(root);

        _optimalIkPos[i] = new DistanceGauge(40);
        name = "optimalIkPos";
        name.append(numberStr);
        _optimalIkPos[i]->setObjectName(name);
        _optimalIkPos[i]->setParentObject(root);

        QVector3D lineStart = QVector3D(_IK_end[i]->_mModel.data()[12],0,_IK_end[i]->_mModel.data()[14]);
        QVector3D lineEnd = QVector3D(_optimalIkPos[i]->_mModel.data()[12],0,_optimalIkPos[i]->_mModel.data()[14]);
        _line_ik_to_optimal[i] = new DashedLine(lineStart, lineEnd);
        name = "line_ik_to_optimal";
        name.append(numberStr);
        _line_ik_to_optimal[i]->setObjectName(name);
        _line_ik_to_optimal[i]->setParentObject(root);

        _debugLoc[i] = new Locator(30,QVector3D(0.2,0.2,0.8));
        //_debugLoc[i]->setParentObject(_legRestAngleGrp[i]);

        _debugLocB[i] = new Locator(30,QVector3D(0.2,0.2,0.8));
        //_debugLocB[i]->setParentObject(_legRestAngleGrp[i]);
    }

    QVector3D tempColor = QVector3D( 0.5, 0.3, 0.6 );
    _leg[0]->setLegColor(tempColor);
    _leg[1]->setLegColor(tempColor);

    update_IK_end();
    update_optimalIkPos();

    for(int i=0; i<_legAmount; i++)
    {
        QVector3D ik_end_Pos;
        ik_end_Pos = getPos(_IK_end[i]->_mModel);
        QVector3D ik_optimal_Pos;
        ik_optimal_Pos = getPos(_optimalIkPos[i]->_mModel);
        QVector3D distToCover;
        distToCover = ik_optimal_Pos - ik_end_Pos;
        _stepCurve[i] = new StepCurve( root, ik_end_Pos, ik_optimal_Pos, distToCover );
    }

    _stepTempo = 300;
    _stepPhase = 0;
    _stepTimer.start();
    _stepTimerAlreadyElapsed = 0;
    _stepTimerRunning = true;

}



void Spider::setSegALength(float value)
{
    _segLength[0] = value;
    //qDebug() << "segment A legth changed to: " << _segLength[0];

    for(int i=0; i<_legAmount; i++)
    {
        _leg[i]->resizeSegA(value);
    }
    //update_IK_end();
}

void Spider::setSegBLength(float value)
{
    _segLength[1] = value;
    //qDebug() << "segment B legth changed to: " << _segLength[1];

    for(int i=0; i<_legAmount; i++)
    {
        _leg[i]->resizeSegB(value);
    }
    //update_IK_end();
}

void Spider::setSegCLength(float value)
{
    _segLength[2] = value;
    //qDebug() << "segment C legth changed to: " << _segLength[2];

    for(int i=0; i<_legAmount; i++)
    {
        _leg[i]->resizeSegC(value);
    }
    //update_IK_end();
}

void Spider::setRootXPos(int value)
{
    _legRootPos.setX(value);
    //qDebug() << "rootXPos changed to: " << _legRootPos.x();

    for(int i=0; i<_legAmount; i++)
    {
        _legRootGrp[i]->_mModel.setToIdentity();
        _legRootGrp[i]->_mModel.translate( pow(-1,i) * _legRootPos.x(),0, _legRootPos.z() * (-2 * (i/2) +1 ) );
    }
    //update_IK_end();
}

void Spider::setRootZPos(int value)
{
    _legRootPos.setZ(value);
    //qDebug() << "rootZPos changed to: " << _legRootPos.z();

    for(int i=0; i<_legAmount; i++)
    {
        _legRootGrp[i]->_mModel.setToIdentity();
        _legRootGrp[i]->_mModel.translate( pow(-1,i) * _legRootPos.x(),0, _legRootPos.z() * (-2 * (i/2) +1 ) );
    }
    //update_IK_end();
}

void Spider::setRootRestAngle(int value)
{
    _legRestAngle = (float)value;
    //qDebug() << "rootRestAngle changed to: " << _legRestAngle;

    for(int i=0; i<_legAmount; i++)
    {
        QMatrix4x4 tempMatrix;
        tempMatrix.setToIdentity();
        tempMatrix.rotate(_legRestAngle+(i/2)*180, 0, (float)pow(-1,i)*(-2*(i/2)+1), 0);
        _legRestAngleGrp[i]->_mModel = tempMatrix;
    }
    //update_IK_end();
}

void Spider::setBodyHeight(int value)
{
    _spiderPos.setY(value);
    qDebug() << "bodyHeight changed to: " << _spiderPos.y();
}

int Spider::getSegALength() const
{
    return _segLength[1];
}

int Spider::getSegCLength() const
{
    return _segLength[2];
}

int Spider::getRootXPos() const
{
    return _legRootPos.x();
}

int Spider::getRootZPos() const
{
    return _legRootPos.z();
}

int Spider::getRootRestAngle() const
{
    return _legRestAngle;
}

int Spider::getBodyHeight() const
{
    return _spiderPos.y();
}

void Spider::rotate(QVector3D angle)
{
    //qDebug() << "spider rotate";
    QMatrix4x4 tempMatrix;
    tempMatrix.setToIdentity();
    tempMatrix.rotate(-angle.x(), 1, 0, 0);
    _bodyGrp->_mModel = _bodyGrp->_mModel * tempMatrix;
    tempMatrix.setToIdentity();
    tempMatrix.rotate(-angle.y(), 0, 1, 0);
    //_body->_mModel.rotate(angle.x(),1,0,0);
    //_body->_mModel.rotate(angle.y(),0,1,0);
    _bodyGrp->_mModel = _bodyGrp->_mModel * tempMatrix;
}

void Spider::move(QVector3D moveVector)
{
    _mBodyPrev = _bodyGrp->_mModel;
    _bodyGrp->_mModel.translate(moveVector);
}

void Spider::resetBody()
{
    _bodyGrp->_mModel.setToIdentity();
    _bodyGrp->_mModel.translate(0,_legRootPos.y(),0);

}

void Spider::toggleTimer()
{
    if(_stepTimerRunning)
    {
        _stepTimerAlreadyElapsed += _stepTimer.elapsed();
        _stepTimerRunning = false;
    }
    else
    {
        _stepTimer.restart();
        _stepTimerRunning = true;
    }
}

QVector3D Spider::solveIK()
{
    //QVector3D resultVec; // X - segment 0 ; Y - segment 1 ; Z - segment 2

    for(int i=0; i<_legAmount; i++)
    {
        QMatrix4x4 legRootInverseTransform;
        legRootInverseTransform.setToIdentity();
        legRootInverseTransform = legRootInverseTransform * _bodyGrp->_mModel;
        legRootInverseTransform = legRootInverseTransform * _legRootGrp[i]->_mModel;
        legRootInverseTransform = legRootInverseTransform * _legRestAngleGrp[i]->_mModel;
        legRootInverseTransform = legRootInverseTransform.inverted();

        QVector3D IK_world_pos = QVector3D(_IK_end[i]->_mModel.data()[12], _IK_end[i]->_mModel.data()[13], _IK_end[i]->_mModel.data()[14] );
        QVector3D IK_local_pos = legRootInverseTransform.map(IK_world_pos);

        _debugLoc[i]->_mModel.setToIdentity();
        _debugLoc[i]->_mModel.translate(IK_local_pos); // --- pos of the IK end effector in local space

        float rootAngleRad = atan2(IK_local_pos.x(),IK_local_pos.z());
        float rootAngleDeg = (rootAngleRad/M_PI)*180;
        _leg[i]->setRotation(0,rootAngleDeg);


        QVector3D rootTipVec = IK_local_pos;
        rootTipVec.setY(0);
        rootTipVec.normalize();
        rootTipVec = rootTipVec * _segLength[0];

        QVector3D rootTipToEnd = IK_local_pos-rootTipVec;
        float sideA = _segLength[1];
        float sideB = _segLength[2];
        float sideC = rootTipToEnd.length();

        float triHeight;
        triHeight = sqrt((sideA+sideB+sideC) * (-sideA+sideB+sideC) * (sideA-sideB+sideC) * (sideA+sideB-sideC)) / (2 * sideC);

        float mPointDist;
        mPointDist = sqrt( pow(sideA,2) - pow(triHeight,2) );

        QVector3D rootTipFlatVec = rootTipToEnd;
        rootTipFlatVec.setY(0);
        float midBaseAngle = atan2(rootTipToEnd.y(),rootTipFlatVec.length());

        float midAngleRad;
        midAngleRad = asin( triHeight / sideA );
        float rightTriangleLength = sqrt( pow(sideB,2) - pow(sideA,2) );
        //if( sideC < rightTriangleLength  )
        //{
        //    midAngleRad = M_PI-midAngleRad;
        //    //qDebug() << "sideA:" << sideA << "sideB:" << sideB << "sideC:" << sideC << "<->" << rightTriangleLength;
        //    //qDebug() << rightTriangleLength;
        //}
        midAngleRad += midBaseAngle;
        float midAngleDeg = (midAngleRad/M_PI)*180;




        float endBaseAngleRad = acos(triHeight/sideA);
        float endAngleRad = acos(triHeight/sideB);
        endAngleRad +=endBaseAngleRad;

        //if( sideC < rightTriangleLength  )
        //{
        //    endAngleRad =  M_PI-endAngleRad - M_PI/2;
        //    //qDebug() << "sideA:" << sideA << "sideB:" << sideB << "sideC:" << sideC << "<->" << rightTriangleLength;
        //    //qDebug() << rightTriangleLength;
        //}

        float endAngleDeg = (endAngleRad/M_PI)*180;


        _leg[i]->setRotation(1,-midAngleDeg);
        _leg[i]->setRotation(2, 180-endAngleDeg);


        _debugLocB[i]->_mModel.setToIdentity();
        _debugLocB[i]->_mModel.translate(rootTipVec);
    }

    update_optimalIkPos();
    for(int i=0; i<_legAmount; i++)
    {
        QVector3D lineStart = QVector3D(_IK_end[i]->_mModel.data()[12],0,_IK_end[i]->_mModel.data()[14]);
        QVector3D lineEnd = QVector3D(_optimalIkPos[i]->_mModel.data()[12],0,_optimalIkPos[i]->_mModel.data()[14]);

        _line_ik_to_optimal[i]->updateLine(lineStart,lineEnd);
    }


    // ---------- stepPhase calculation ----------
    if( (_stepTimer.elapsed()+_stepTimerAlreadyElapsed) > _stepTempo )
    {
        _stepTimerAlreadyElapsed = (_stepTimer.elapsed()+_stepTimerAlreadyElapsed) -_stepTempo;
        _stepTimer.restart();
    }
    _stepPhase = (_stepTimer.elapsed()+_stepTimerAlreadyElapsed) / _stepTempo ;
    // ---------- stepPhase calculation ----------END


    // ---------- stest stepping motion ----------
    if(_stepTimerRunning)
    {
        for(int i=0; i<_legAmount; i++)
        {
            float tempPhase = _stepPhase + (i==1 || i==2 ? 0:0.5 );

            QVector3D ik_pos = QVector3D(_IK_end[i]->_mModel.data()[12],_IK_end[i]->_mModel.data()[13],_IK_end[i]->_mModel.data()[14]);
            _IK_end[i]->_mModel.setToIdentity();
            _IK_end[i]->_mModel.translate(ik_pos.x(),abs(cos(tempPhase*M_PI)*10),ik_pos.z());
        }
    }
    // ---------- stest stepping motion ----------END
}

QString Spider::getName()
{
    return _name;
}

QVector3D Spider::getPos(QMatrix4x4 matrix)
{
    QVector3D result;

    result.setX(matrix.data()[12]);
    result.setY(matrix.data()[13]);
    result.setZ(matrix.data()[14]);

    return result;
}

void Spider::update_IK_end()
{
    for(int i=0; i<_legAmount; i++) // --- for the IK end effectors
    {
        _IK_end[i]->_mModel.setToIdentity();
        _IK_end[i]->_mModel = _IK_end[i]->_mModel * _bodyGrp->_mModel;
        _IK_end[i]->_mModel = _IK_end[i]->_mModel * _legRootGrp[i]->_mModel;
        _IK_end[i]->_mModel = _IK_end[i]->_mModel * _legRestAngleGrp[i]->_mModel;
        _IK_end[i]->_mModel.translate(0,0,_segLength[0]);

        QMatrix4x4 tempMat;
        tempMat.setToIdentity();
        float * tempData = _IK_end[i]->_mModel.data();
        _IK_end[i]->_mModel.translate(0, 0, sqrt( pow(tempData[13],2) + pow( ((_segLength[1]+_segLength[2])/4) ,2) ) );
        tempData[13] = 0;

        _IK_end[i]->_mModel = _IK_end[i]->_mModel * _legRestAngleGrp[i]->_mModel.inverted();

    }
}

void Spider::update_optimalIkPos()
{
    for(int i=0; i<_legAmount; i++)
    {
        QMatrix4x4 legRootInverseTransform;
        legRootInverseTransform.setToIdentity();
        legRootInverseTransform = legRootInverseTransform * _bodyGrp->_mModel;
        legRootInverseTransform = legRootInverseTransform * _legRootGrp[i]->_mModel;
        legRootInverseTransform = legRootInverseTransform * _legRestAngleGrp[i]->_mModel;
        //legRootInverseTransform = legRootInverseTransform.inverted();

        legRootInverseTransform.translate(0,0,_segLength[0]);

        QVector3D segTip;
        segTip = legRootInverseTransform.map(QVector3D(0,0,0));

        float sideA = segTip.y();
        float sideC = _segLength[1]+_segLength[2]; //farthest the leg can reach
        float angle = acos(sideA/sideC);
        float sideB = sin(angle)*sideC; // horizontal component of farthest distance

        //qDebug()<<sideA;
        _optimalIkPos[i]->setRadius(sideB/2);
        //_optimalIkPos[i]->setRadius(10*i+10);

        QMatrix4x4 farthestIkMatrix = legRootInverseTransform;
        farthestIkMatrix.translate(0,0,sideB);
        QVector3D farthestlIkPos;
        farthestlIkPos = farthestIkMatrix.map(QVector3D(0,0,0));
        farthestlIkPos.setY(0);

        QVector3D tipToFarthest = farthestlIkPos-segTip;
        QVector3D offset = tipToFarthest;
        offset.setY(0);
        offset.normalize();
        offset = offset * (sideB/2);

        QVector3D optimal;
        optimal = segTip + offset;
        optimal.setY(0);

        //_optimalIkPos[i]->_mModel.data()[12] = legRootInverseTransform.data()[12];
        //_optimalIkPos[i]->_mModel.data()[13] = legRootInverseTransform.data()[13];
        //_optimalIkPos[i]->_mModel.data()[14] = legRootInverseTransform.data()[14];
        _optimalIkPos[i]->_mModel.setToIdentity();
        _optimalIkPos[i]->_mModel.translate(optimal);
    }
}

void Spider::update_stepCurve()
{

}
