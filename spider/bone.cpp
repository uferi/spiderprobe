#include "bone.h"

#include "scenemanager.h"

Bone::Bone(float length)
{
    //_length = length;
    //_lengthRatio =  0.1;
    //_lengthRatio *= _length;
    //_widthRatio = 20;
    //_heightRatio = 0.25;
    //_heightRatio *= _length;

    _length = length;
    _lengthRatio =  5;
    _widthRatio = 20;
    _heightRatio = 10;

    _color = QVector3D( 0.8, 0.8, 0.8 );

    createGeometryData(_color);
}

Bone::Bone(float length, QVector3D color)
{
    _length = length;
    _lengthRatio =  5;
    _widthRatio = 20;
    _heightRatio = 10;

    _color = color;

    createGeometryData(_color);
}

void Bone::initObject()
{
    SceneManager& scnMng = SceneManager::instance();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();
    glFunc->glGenBuffers(1, &_VBO);
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glBufferData(GL_ARRAY_BUFFER, _data.size()*4, _data.data(), GL_DYNAMIC_DRAW);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)12);
    glFunc->glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)24);

    _shaderProgram = scnMng.getShader(ShaderType::BONE);

    for(int i=0; i<_child.length(); i++)
    {
        _child[i]->initObject();
    }

}

void Bone::paintObject(QMatrix4x4 parentMatrix)
{
    //qDebug() << "the bone draws itself";
    SceneManager& scnMng = SceneManager::instance();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();

    glFunc->glUseProgram(_shaderProgram);

    // bind VBO first !!!
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)12);
    glFunc->glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)24);
    glFunc->glEnableVertexAttribArray(0);
    glFunc->glEnableVertexAttribArray(1);
    glFunc->glEnableVertexAttribArray(2);

    QMatrix4x4 projMatrix = scnMng.getProjMatrix();
    QMatrix4x4 modelMatrix;
    QMatrix4x4 normalMatrix;

    unsigned int modelMatrixLoc;
    unsigned int normalMatrixLoc;
    unsigned int camMatrixLoc;
    unsigned int projMatrixLoc;

    modelMatrix =  parentMatrix * _mModel;
    normalMatrix = modelMatrix;
    normalMatrix.setColumn(3, QVector4D(0, 0, 0, 1) ); // kill translation - keep only rotation

    modelMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"modelMatrix");
    normalMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"normalMatrix");
    camMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"camMatrix");
    projMatrixLoc = glFunc->glGetUniformLocation(_shaderProgram,"projMatrix");

    Camera* renderCamera = scnMng.getRenderCamera();
    QMatrix4x4 camMatrix;
    camMatrix = renderCamera->getMatrix();

    glFunc->glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, (GLfloat*)&modelMatrix );
    glFunc->glUniformMatrix4fv(normalMatrixLoc, 1, GL_FALSE, (GLfloat*)&normalMatrix );
    glFunc->glUniformMatrix4fv(camMatrixLoc, 1, GL_FALSE, (GLfloat*)&camMatrix );
    glFunc->glUniformMatrix4fv(projMatrixLoc, 1, GL_FALSE, (GLfloat*)&projMatrix);

    //glFunc->glLineWidth(1);

    //glDrawArrays(GL_TRIANGLES, 0, 3);
    glFunc->glDrawArrays(GL_TRIANGLES, 0, _data.size()/9);
    //glPointSize(5.0);
    //glDrawArrays(GL_POINTS, 0, scnMng.nodeList[0]->_data.size()/5);

    QMatrix4x4 localMatrix;
    localMatrix = parentMatrix * _mModel;
    localMatrix.translate(0,0,_length);
    for(int i=0; i<_child.length(); i++)
    {
        _child[i]->paintObject(localMatrix);
    }
}

//void Bone::setParentObject(Object3D *parent)
//{
//    _parent = parent;
//}

float Bone::getLength()
{
    return _length;
}

void Bone::resizeBone(float length)
{
    _data.clear();

    _length = length;

    createGeometryData(_color);

    //SceneManager& scnMng = SceneManager::instance();

    QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();
    //glFunc->glGenBuffers(1, &_VBO);
    glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glFunc->glBufferData(GL_ARRAY_BUFFER, _data.size()*4, _data.data(), GL_DYNAMIC_DRAW);
    glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)0);
    glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)12);
    glFunc->glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)24);

}

void Bone::setColor(QVector3D color)
{
    _data.clear();

    _color = color;

    createGeometryData(_color);

    //SceneManager& scnMng = SceneManager::instance();

    //QOpenGLFunctions *glFunc = QOpenGLContext::currentContext()->functions();
    //
    //glFunc->glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    //glFunc->glBufferData(GL_ARRAY_BUFFER, _data.size()*4, _data.data(), GL_DYNAMIC_DRAW);
    //glFunc->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)0);
    //glFunc->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)12);
    //glFunc->glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)24);
}

void Bone::addPoint(QVector3D pos, QVector3D normal, QVector3D col)
{
    _data.push_back( pos.x() );
    _data.push_back( pos.y() );
    _data.push_back( pos.z() );

    _data.push_back( normal.x() );
    _data.push_back( normal.y() );
    _data.push_back( normal.z() );

    _data.push_back( col.x() );
    _data.push_back( col.y() );
    _data.push_back( col.z() );
}

QVector3D Bone::faceNormal(QVector3D p0, QVector3D p1, QVector3D p2)
{
    QVector3D resultVec;
    QVector3D aVec = p1 - p0;
    QVector3D bVec = p2 - p0;
    aVec.normalize();
    bVec.normalize();
    resultVec = QVector3D::crossProduct( bVec, aVec );

    return resultVec;
}

void Bone::createGeometryData(QVector3D color)
{
    QVector3D normal;

    //QVector3D color = QVector3D( 0.25, 0.25, 0.25 );
    //color = QVector3D( 0.25, 0.25, 0.25 );

    QVector3D p0 = QVector3D( 0, 0, 0 );

    QVector3D p1 = QVector3D( - _length/_widthRatio, 0, _length/_lengthRatio );
    QVector3D p2 = QVector3D( 0, _length/_heightRatio, _length/_lengthRatio );
    QVector3D p3 = QVector3D(   _length/_widthRatio, 0, _length/_lengthRatio );
    QVector3D p4 = QVector3D( 0, - _length/_heightRatio, _length/_lengthRatio );

    QVector3D p5 = QVector3D( 0, 0, _length );

    //1
    normal =  faceNormal( p0, p2, p1 );
    addPoint( p0, normal, color );
    addPoint( p1, normal, color );
    addPoint( p2, normal, color );
    //2
    normal =  faceNormal( p0, p3, p2 );
    addPoint( p0, normal, color );
    addPoint( p2, normal, color );
    addPoint( p3, normal, color );
    //3
    normal =  faceNormal( p0, p4, p3 );
    addPoint( p0, normal, color );
    addPoint( p3, normal, color );
    addPoint( p4, normal, color );
    //4
    normal =  faceNormal( p0, p1, p4 );
    addPoint( p0, normal, color );
    addPoint( p4, normal, color );
    addPoint( p1, normal, color );

    //5
    normal =  faceNormal( p2, p5, p1 );
    addPoint( p2, normal, color );
    addPoint( p1, normal, color );
    addPoint( p5, normal, color );
    //6
    normal =  faceNormal( p3, p5, p2 );
    addPoint( p3, normal, color );
    addPoint( p2, normal, color );
    addPoint( p5, normal, color );
    //7
    normal =  faceNormal( p4, p5, p3 );
    addPoint( p4, normal, color );
    addPoint( p3, normal, color );
    addPoint( p5, normal, color );
    //8
    normal =  faceNormal( p1, p5, p4 );
    addPoint( p1, normal, color );
    addPoint( p4, normal, color );
    addPoint( p5, normal, color );
}
