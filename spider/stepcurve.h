#ifndef STEPCURVE_H
#define STEPCURVE_H

#include "general/object3d.h"
#include "env/beziercurve.h"

class StepCurve : public Object3D
{
public:
    StepCurve(Object3D *root, QVector3D startPos, QVector3D endPos, QVector3D velocity);

    void updateCurve(QVector3D startPos, QVector3D endPos, QVector3D velocity);

private:
    QVector3D _startPos;
    QVector3D _endPos;
    QVector3D _velocity;

    BezierCurve* _curve[3];

};

#endif // STEPCURVE_H
