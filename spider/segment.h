#ifndef SEGMENT_H
#define SEGMENT_H

#include "general/entity.h"

class Segment : public Entity
{
public:
    Segment();

private:
    float _length;
    float _angle;
    float _restAngle;
    float _minAngle;
    float _maxAngle;
};

#endif // SEGMENT_H
