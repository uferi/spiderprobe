#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in float opacity;

out float opac;

uniform mat4 modelMatrix;
uniform mat4 projMatrix;
uniform mat4 camMatrix;

void main() {

   gl_Position = projMatrix * camMatrix * modelMatrix * vec4(aPos.x, aPos.y, aPos.z, 1.0);
   opac = opacity;

};
