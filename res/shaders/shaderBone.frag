#version 330 core

in highp vec3 vert;
in highp vec4 vertNormal;
in       vec3 vertColor;

uniform vec3 lightPos1 = vec3(1000, 2000, 2000);
uniform vec3 lightPos2 = vec3(-3000, -2000, 2000);

uniform vec3 color1 = vec3( 0.9, 0.9, 0.9 );
uniform vec3 color2 = vec3( 0.12, 0.2, 0.4 );
uniform vec3 amb = vec3( 0.1, 0.1, 0.1 );

out vec4 FragColor;

void main() {
    highp vec3 L1 = normalize(lightPos1 - vert);
    highp vec3 L2 = normalize(lightPos2 - vert);

    highp float NL1= max( dot( vertNormal.xyz, L1), 0.0);
    highp float NL2= max( dot( vertNormal.xyz, L2), 0.0);

    highp vec3 col1 = clamp(color1 * 0.2 + color1 * 0.8 * NL1, 0.0, 1.0);
    highp vec3 col2 = clamp(color2 * 0.3 + color2 * 0.7 * NL2, 0.0, 1.0);

    //highp vec3 col = clamp( (color1 + color2) , vec3(0,0,0), vec3(1,1,1) );


    FragColor = vec4( (col1.r+col2.r)*vertColor.r+amb.r, (col1.g+col2.g)*vertColor.g+amb.g, (col1.b+col2.b)*vertColor.b+amb.b, 1 );
    //FragColor = vec4(vertColor,1);
};
