#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 color;

out vec3 col;

uniform mat4 modelMatrix;
uniform mat4 projMatrix;
uniform mat4 camMatrix;

void main() {

   gl_Position = projMatrix * camMatrix * modelMatrix * vec4(aPos.x, aPos.y, aPos.z, 1.0);
   col = color;

};
