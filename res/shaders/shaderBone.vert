#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec3 aColor;

out vec3 vert;
out vec4 vertNormal;
out vec3 vertColor;
vec4 xformPos;

uniform mat4 modelMatrix;
uniform mat4 normalMatrix;


uniform mat4 projMatrix;
uniform mat4 camMatrix;

void main() {
    xformPos = projMatrix * camMatrix * modelMatrix * vec4(aPos.x, aPos.y, aPos.z, 1.0);
    vert = xformPos.xyz;
    vertNormal = normalize( normalMatrix * vec4(aNormal,1) );
    gl_Position = xformPos;
    vertColor = aColor;
};
