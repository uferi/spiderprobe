#version 330 core

in float opac;

out vec4 FragColor;

void main() {

   FragColor = vec4(0.52f*opac, 0.52f*opac, 0.52f*opac, 1.0);
   //FragColor.a = 0.01;

};
