#version 330 core

in vec3 col;

out vec4 FragColor;

void main() {

   FragColor = vec4(col.r, col.g, col.b, 1.0);
   //FragColor.a = 0.1;

};
