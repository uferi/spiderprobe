#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <vector>

#include <QWidget>
#include <QOpenGLWidget>
#include <QOpenGLContext>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QMouseEvent>
#include <QKeyEvent>

#include "general/object3d.h"

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    explicit GLWidget(QWidget *parent = nullptr);

    void initializeGL();
    void paintGL();
    void resizeGL(int w, int h);

protected:

    void mousePressEvent(QMouseEvent *event ) override;
    void mouseMoveEvent(QMouseEvent *event ) override;
    void keyPressEvent(QKeyEvent *keyEvent) override;
    void keyReleaseEvent(QKeyEvent *keyEvent) override;

signals:

public slots:

private:
    bool isCameraMovable;

    QOpenGLContext *context;
    QOpenGLFunctions *openGLFunctions;
    QPoint _lastPos;

    //QTimer timer, timer2;
    GLuint VBO;
    GLuint vertexShader;
    GLuint fragmentShader;
    GLuint shaderProgram;
    GLuint VAO;

    QMatrix4x4 modelMatrix;
    QMatrix4x4 projMatrix;
    unsigned int modelMatrixLoc;
    unsigned int camMatrixLoc;
    unsigned int projMatrixLoc;
    unsigned int rotation;

    Object3D blob3d;
    std::vector<Object3D> objectList;
};

#endif // GLWIDGET_H
